<?php
	//This is the administration interface for the order tool
	//Please use it only with HTTPS, because otherwise passwords will be
	//transmitted without any encryption, which will cause a security leak
		
	//*******************************************************************************************************************************************************************************************
	//important variables which may be need to be reconfigured when migrating script
	//to other server
	$modules = "order-tool-modules";	//folder containing the modules of the order tool
	$database = "127.0.0.1"; 	//Servername of the database
	$user = "root";				//Username for database connection
	$password = "";				//Password for database connection
	$dbname = "bestelltool";	//Name of the MySQL database storing the script orders
	$dbname2 = "admin";			//Name of the MySQL database storing the login and session information
	$deny_empty = false;		//true, if login shall be denied if the field "Passwort" in the user table is empty -> false: login doesn't require a password for this user
	$script_ack = true;		//true, if all script orders have to be acknowledged by the user before they are printed
	$login_refresh = 5;			//specifies the amount of minutes after which the login of the user shall be refreshed if he has klicked a link or a button (default 5)
	$session_time_out = 1440;	//specifies the amount of minutes after which a session gets invalid if the user was inactive (default 1440 = 24 Hours)
	$beginn_summer_semester = "01.04.";	//specifies the start date of the summer semester
	$beginn_winter_semester = "01.10.";	//specifies the start date of the winter semester
	$from = "";					//this is the mail address to be filled in the FROM-field of the mail which is sent when the user clicks the button "Erinnerungsmail senden" at the page "Archiv"
	//*******************************************************************************************************************************************************************************************
		
	$connection = new mysqli($database, $user, $password, $dbname);
	$connection2 = new mysqli($database, $user, $password, $dbname);
	$connection3 = new mysqli($database, $user, $password, $dbname2); //wird für Login benötigt
	
	if (substr($modules, -1, 1) != "/")
	{
		$separator = "/";
	}
	else
	{
		$separator = "";
	}
	
	unset($database,$user,$password,$dbname,$dbname2);
	
	if (mysqli_connect_errno())
	{
		printf("Connection error: %s\n",mysqli_connect_error());
		exit();
	}
	
	$session_valid = false;
	$admin_right = false;	//Gibt an, ob der aktuelle Benutzer berechtigt ist, andere Benutzerkonten zu verwalten
	$timed_out = false;
			
	$lfn = "";
	$lln = "";
	$invalid_pw = false;
	$invalid_name = false;
	
	$datum = date('Y');
	$timestamp1 = strtotime($beginn_summer_semester.$datum);
	$timestamp2 = strtotime($beginn_winter_semester.$datum);
	$datum1 = date('n',$timestamp1);
	$datum2 = date('n',$timestamp2);	
	if (date('n') >= $datum1 AND date('n') < $datum2)
	{
		$sem = 0;
		$semester = "Sommersemester ".$datum;
	}
	else
	{
		if (date('n') < date('n',strtotime($beginn_summer_semester.$datum)))
			$datum--;
			
		$sem = 1;
		$semester = "Wintersemester ".$datum;
	}
	
	if (!isset($_GET['page']))
		$_GET['page'] = 'login'; //Wenn der Parameter "page" nicht mitgeliefert wurde, so leite auf die Login-Seite um
	
	function gen_salt($length)
	{
		$c = "";
		$a = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789°^!$%&/{([])}=?\*+~#-<>';
		for ($i = 0; $i < $length; $i++)
		{
			$b = mt_rand(0,strlen($a)-1);
			$c = $c.substr($a,$b,1);
		}
		return $c;
	}
		
	if (isset($_POST['login'])) //wird ausgeführt, wenn der Button auf der Login-Seite gedrückt wurde
	{
		$lfn = trim($_POST['firstname']);
		$lln = trim($_POST['lastname']);
		$lfn = mysqli_real_escape_string($connection,$lfn);
		$lfn = str_replace("'",'',$lfn);
		$lfn = strip_tags($lfn);
		$lln = mysqli_real_escape_string($connection,$lln);
		$lln = str_replace("'",'',$lln);
		$lln = strip_tags($lln);
		
		
		//Wenn der Benutzer auf den Login-Button gedrückt hat, prüfe die Eingabe
		$query = "".
		"SELECT ID, Passwort, Salt ".
		"FROM user ".
		"WHERE Vorname = '".$lfn."' ".
		"AND Nachname = '".$lln."'";
						
		$result = $connection3->query($query);
		if ($row = $result->fetch_assoc())
		{
			$pw = $row['Passwort'];
			$salt = $row['Salt'];
			$salt = str_replace(' ','',$salt);
						
			function create_session()
			{
				global $row,$_GET,$session_valid,$connection3,$sessionid;
				$uid = $row['ID'];
				//Benutzer ohne Passwort-Hash darf sich anmelden -> Lösche eventuell noch vorhandene Sessions dieser Benutzer-ID
				//und erzeuge anschließend eine neue Session
				$query = "
				DELETE FROM session
				WHERE userid = '".$uid."'
				";
				
				$result = $connection3->query($query);
					
				//Session-ID generieren
				$sessionid_exists = true;
				while ($sessionid_exists == true)
				{
					$sessionid = mt_rand();
					$query = "
					SELECT sessionid
					FROM session
					WHERE sessionid = '".$sessionid."'";
					
					$result = $connection3->query($query);
					if (!($row = $result->fetch_assoc()))
						$sessionid_exists = false;
				}
				
				//Nun wird die neue Session generiert
				$query = "
				INSERT INTO session
				VALUES ('".$sessionid."','".$uid."',NOW())
				";
					
				$result = $connection3->query($query);
				
				setcookie('session',$sessionid)	;
				$session_valid = true;
				$_COOKIE['session'] = $sessionid; //Cookie setzen, das die aktuelle Sitzung identifiziert
				$_GET['page'] = 'overview';
			}
			
			//Der eingegebene Name wurde in der Datenbank gefunden, also prüfe nun das Passwort
			if ($pw == "")
			{
				//für diesen Nutzer ist kein Passwort-Hash eingetragen
				if ($deny_empty == false)
				{
					create_session();
				}
			}
			else
			{
				//Passwort-Hash prüfen
				$pwhash = hash("sha512",$_POST['password'].$salt);
				
				if ($pwhash == $row['Passwort'])
				{
					create_session();
				}
				else
				{
					$invalid_pw = true;
				}
			}
		}
		else
		{
			$invalid_name = true;
		}
	}
	
	if (isset($_COOKIE['session'])) //prüft, ob die Sitzung gültig ist
	{		
		$timed_out = false;
		$_COOKIE['session'] = mysqli_real_escape_string($connection,$_COOKIE['session']);
		$_COOKIE['session'] = str_replace('"','',$_COOKIE['session']);
		$_COOKIE['session'] = strip_tags($_COOKIE['session']);
							
		//Prüft, ob die übermittelte SessionID gültig ist.
		$query = "".
		"SELECT sessionid, userid, UNIX_TIMESTAMP(logindatum) AS login ".
		"FROM Session ".
		"WHERE sessionid = '".$_COOKIE['session']."'";
				
		$result = $connection3->query($query);
					
		if ($row = $result->fetch_assoc())
		{
			$now = time();
			$ltime = $row['login'];
			$session_time_out = $session_time_out*60;
			$diff = $now-$ltime;
			
			if ($diff > $session_time_out)
			{
				//Wenn die Session-ID bereits abgelaufen ist,
				//lösche die Session aus der Datenbank
				$query = "
				DELETE FROM session
				WHERE sessionid = '".$_COOKIE['session']."'
				";
				
				$result = $connection3->query($query);
				$_GET['page'] = 'login';
				$timed_out = true;
			}
			else
			{
				//Wenn die Session-ID gültig ist,
				//ermögliche Zugriff auf gesperrte Seiten
				$session_valid = true;
				$sessionid = $row['sessionid'];
				$uid = $row['userid'];
			
				//Login auffrischen, falls erforderlich
				$login_refresh = $login_refresh*60;
				if ($diff > $login_refresh)
				{
					//Die Sitzung muss aufgefrischt werden
					$query = "
					UPDATE session SET logindatum=NOW()
					WHERE userid='".$uid."'
					";
					
					$result = $connection3->query($query);
				}
			
				//Abfrage, ob der Nutzer Admin-Recht hat
				$query = "
				SELECT admin
				FROM user
				WHERE id = '".$uid."'
				";
			
				$result = $connection3->query($query);
				if ($row = $result->fetch_assoc())
				{
					if ($row['admin'] == 1)
						$admin_right = true;
				}
			}
		}
	}

	if ($_GET['page'] == 'logout' AND $session_valid == true) //Logout
	{
		$query = "
		DELETE FROM session
		WHERE userid='".$uid."'
		";
				
		$result = $connection3->query($query);
		$session_valid = false;
		$_GET['page'] = 'login';
		setcookie('session',''); //Coolie löschen
		$lfn = "";
		$lln = "";
		$invalid_pw = false;
		$invalid_name = false;
	}
	
	if (isset($_POST['chpw']) AND $session_valid == true) //wird ausgeführt, wenn der Button zum Ändern des eigenen Passwortes gedrückt wurde
	{
		$old_pw = $_POST['old'];
				
		$query = "
		SELECT Salt, Passwort
		FROM session INNER JOIN user
		ON session.userid = user.ID
		WHERE sessionid = ".$sessionid."
		";
		
		$result = $connection3->query($query);
		if ($row = $result->fetch_assoc())
		{
			$old_pw = "";
			if ($_POST['old'] != "")
			{
				$old_pw = hash('sha512',$_POST['old'].$row['Salt']);
			}
			if ($row['Passwort'] == $old_pw)
			{
				if ($_POST['password'] == $_POST['password2'])
				{
					if ($_POST['password'] != "")
					{
						$salt = gen_salt(15);
						$pw = hash('sha512',$_POST['password'].$salt);
					}
					else
					{
						$salt = '';
						$pw = '';
					}
		
					$query = "
					UPDATE user SET salt='".$salt."', passwort='".$pw."'
					WHERE id='".$uid."'
					";
		
					$result = $connection3->query($query);
					if ($result == false)
						$pw_ch = false;
					else
						$pw_ch = true;
				}
				else
				{
					$pw_ch = false;
					$not_identic = true;
				}
			}
			else
			{
				$invalid_pw = true;
				$pw_ch = false;
			}
		}
	}
		
	echo "
	<!DOCTYPE html>
	<html>
		<head>
			<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
			<title>Fachschaftsrat IMN: Skriptbestellung - Admin</title>
			<meta name=\"keywords\" content=\"Fachschaftsrat, FSR, IMN, HTWK, Leipzig, Informatik, Mathematik, Medieninformatik\">
			<meta name=\"author\" content=\"Marvin Haagen\">
			<meta name=\"date\" content=\"".date('d.m.Y H:i:s',getlastmod())."\">
			
			<style type=\"text/css\">";
				
				echo "
".file_get_contents($modules.$separator."admin-styles.css");
				
				echo "
			</style>
		</head>
		<body>
			<div id=\"navigation\">
				<p>Navigation</p>
				<ul>";
												
				$styles = array(
				 'overview' => 'navigation',
				 'script' => 'navigation',
				 'dozent' => 'navigation',
				 'customer' => 'navigation',
				 'curse' => 'navigation',
				 'archives' => 'navigation',
				 'optimize' => 'navigation',
				 'import' => 'navigation',
				 'export' => 'navigation',
				 'user' => 'navigation',
				 'sessions' => 'navigation',
				 'password' => 'navigation',
				 'login' => 'navigation',
				 'info' => 'navigation'
				);
				
				switch ($_GET['page'])
				{
					case 'overview':
						$styles['overview'] = 'navi_active';
						break;
					case 'script':
						$styles['script'] = 'navi_active';
						break;
					case 'dozent':
						$styles['dozent'] = 'navi_active';
						break;
					case 'customer':
						$styles['customer'] = 'navi_active';
						break;
					case 'curse':
						$styles['curse'] = 'navi_active';
						break;
					case 'archives':
						$styles['archives'] = 'navi_active';
						break;
					case 'optimize':
						$styles['optimize'] = 'navi_active';
						break;
					case 'import':
						$styles['import'] = 'navi_active';
						break;
					case 'export':
						$styles['export'] = 'navi_active';
						break;	
					case 'login':
						$styles['login'] = 'navi_active';
						break;
					case 'password':
						$styles['password'] = 'navi_active';
						break;	
					case 'user':
						$styles['user'] = 'navi_active';
						break;
					case 'sessions':
						$styles['sessions'] = 'navi_active';
						break;
					case 'info':
						$styles['info'] = 'navi_active';
				}
				
				//Hier werden jetzt die Navigationslinks generiert
				echo "
				<li><a class=\"".$styles['overview']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=overview\">Skriptbestellungen</a></li>
				<li><a class=\"".$styles['script']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=script\">Skripte</a></li>
				<li><a class=\"".$styles['dozent']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=dozent\">Dozenten</a></li>
				<li><a class=\"".$styles['customer']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=customer\">Besteller</a></li>
				<li><a class=\"".$styles['curse']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=curse\">Studiengänge</a></li>
				<li><a class=\"".$styles['archives']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=archives\">Archiv</a></li><hr>";
				/* wird in Version 1.1 hinzugefügt
				<li><a class=\"".$styles['optimize']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=optimize\">Datenbank optimieren</a></li>
				<li><a class=\"".$styles['import']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=import\">Datenbank importieren</a></li>
				<li><a class=\"".$styles['export']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=export\">Datenbank exportieren</a></li><hr>";*/
				
				if ($session_valid == true)
				{	
					//Wenn der Benutzer eingeloggt ist:
					if ($admin_right == true)
					{
						//Wenn der Nutzer Admin-Recht hat, werden die Links zum Verwalten der Benutzer und Sitzungen angezeigt
						echo "
				<li><a class=\"".$styles['user']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=user\">Benutzerkonten</a></li>
				<li><a class=\"".$styles['sessions']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=sessions\">Sitzungen</a></li>";
					}
				}
				
				echo "<li><a class=\"".$styles['info']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=info\">Infos zum Bestelltool</a></li>";
				
				if ($session_valid == true)
				{
					echo "
				<li><a class=\"".$styles['password']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=password\">Passwort ändern</a></li>
				<li><a class=\"navigation\" href=\"".$_SERVER['SCRIPT_NAME']."?page=logout\">Logout</a></li>";
				}
				else
				{
					echo "
				<li><a class=\"".$styles['login']."\" href=\"".$_SERVER['SCRIPT_NAME']."?page=login\">Login</a></li>";
				}
								
				unset($styles);
				
				echo "
				</ul>
			</div>
			<div id=\"content\">";
			//Hier erfolgt die Generierung der Seite
			switch ($_GET['page'])
			{
				case 'login': //Login-Seite	
					include $modules.$separator."login.php";
					break;
				case 'password': //Ändern des eigenen Passwortes
					include $modules.$separator."chpasswd.php";
					break;
				case 'overview': //Übersicht über bestellte Skripte
					include $modules.$separator."overview.php";
					break;
				case 'archives': //Übersicht über nicht bestätigte Bestellungen
					include $modules.$separator."archives.php";
					break;
				case 'customer': //Übersicht aller Besteller
					include $modules.$separator."customer.php";
					break;
				case 'script': //Ändern der im System hinterlegten Skripte
					include $modules.$separator."script.php";
					break;
				case 'dozent': //Ändern der im System hinterlegten Dozenten
					include $modules.$separator."dozent.php";
					break;
				case 'curse': //Ändern der Studiengänge und der zugeordneten Skripte
					include $modules.$separator."curse.php";
					break;
				case 'info': //Anzeigen der allgemeinen Infos zum Projekt
					include $modules.$separator."info.php";
					break;
			}
			echo "
			</div>
		</body>
	</html>";
?>