<?php
	echo "
			<div class=\"welcome_page\">
				<h1>Skripte für das ".$semester."</h1>
				<p>Euer Fachschaftsrat IMN bietet euch aktuelle veranstaltungsbegleitende
				Materialien an. Diese Materialien sind u.a. Folien, Skripte und Zusammenfassungen
				für Veranstaltungen, welche im jeweils befindlichen Semester gehalten werden. Da
				es sich hierbei um Lehrmaterialien handelt, sind die Inhalte nur im akademischen
				Umfeld verfügbar, d.h. nicht öffentlich.<br>
				Wir bieten euch die veranstaltungsbegleitenden Materialien und alle anderen Kopien
				zum Selbstkostenpreis (Break-even-Point) an, also für jeden erschwinglich. Somit
				kommt ihr sehr wahrscheinlich nicht billiger an eine gedruckte Version der in
				elektronischer Form zur Verfügung gestellten Materialien. Außerdem möchten wir auf
				die Nutzungsordnung der Pools hinweisen, in denen der Druck von Skripten untersagt
				ist.<br><br>
				Je nachdem mit welcher Auffassung ein Dozent seine Lehrveranstaltung organisiert,
				existieren veranstaltungsbegleitende Materialien. Hierbei gibt es Unterschiede zwischen
				vorgefertigten, parallel zu Veranstaltungen aktualisierten oder keinen Materialien.
				Bei den folgenden Dozenten sind keine Materialien für die Veranstaltungen vorgesehen:</p>
				<ul>";
		
	//display tutors who does not provide scripts
	$query = "
	SELECT DISTINCT Name
	FROM Dozent
	WHERE DID NOT IN 
	(SELECT Dozent.DID AS DID
	FROM Dozent INNER JOIN Skript
	ON Dozent.DID = Skript.DID)
	AND pAktualisierung = 0
	ORDER BY Name ASC";
	$result = $connection->query($query);
	while ($row = $result->fetch_assoc())
	{
		echo "
					<li>".$row['Name']."</li>";
	}
		
	//display tutors who partially update their scripts
	echo "
				</ul><p><br>
				Die nachfolgenden Dozenten aktualisieren ihre veranstaltungsbegleitenden Materialien während
				des Semesters partiell, sodass deren Materialien periodisch zur Verfügung gestellt werden
				können (auf Anfrage). Sie sind auf den Dozentenhomepages oder im Opal zu finden.</p>
				<ul>";
		
	$query = "
	SELECT DISTINCT Name 
	FROM Dozent
	WHERE pAktualisierung = 1
	ORDER BY Name ASC";
	$result = $connection->query($query);
	while ($row = $result->fetch_assoc())
	{
		echo "
					<li>".$row['Name']."</li>";
	}
		
			echo "
				</ul><br><br>
				<table>
				<colgroup>
					<col>
					<col>
					<col>
					<col>
				</colgroup>
				<tr>
					<th>Dozent&#160;&#160;</th>
					<th>Fach&#160;&#160;</th>
					<th>Semester&#160;&#160;</th>
					<th>Kosten&#160;&#160;</th>
				</tr>";
		
	//display available scripts
	$query = "
	SELECT DISTINCT Modulbezeichnung, CONCAT(FORMAT(Skript.Preis,2,'de_DE'),' €') AS Preis, Semester, Name
	FROM Skript INNER JOIN Dozent
	ON Skript.DID = Dozent.DID
	WHERE Verfuegbar = 1 ";
			
	if ($sem == 0)
	{
		$cond = "AND Sommersemester = 1";
	}
	else
		$cond = "AND Wintersemester = 1";
			
	$query = $query.$cond." ORDER BY Semester ASC, Modulbezeichnung ASC";
			
	$result = $connection->query($query);
	while ($row = $result->fetch_assoc())
	{
		echo "
					<tr><td>".$row['Name']."&#160;&#160;</td><td>".$row['Modulbezeichnung']."&#160;&#160;</td><td>".$row['Semester']."&#160;&#160;</td><td>".$row['Preis']."&#160;&#160;</td></tr>";
	}
		
	echo "
				</table><p><br>
				&sup1; ... Dokument vom Vorjahr<br>
				<br><br>
				Dieses Semester könnt ihr wieder Skripte <a href=\"".$_SERVER['SCRIPT_NAME']."?page=order\">online vorbestellen</a>, damit wir diese f&uuml;r euch
				bereitlegen können. Wie weit eure Bestellung bereits ist, könnt ihr unter <a href=\"".$_SERVER['SCRIPT_NAME']."?page=status\">Statuspr&uuml;fung</a> einsehen. Wenn eure Bestellung als gedruckt
				markiert ist, bitten wir euch, diese innerhalb von zwei Wochen abzuholen.<br>
				<br>
				Bei Fragen oder Problemen könnt ihr diese an 
				<a href=\"javascript:decode_uri();\">".str_replace('@',' (at) ',$contact)."</a> senden.<br>
				<br>
				Wir investieren viel Aufwand in die Organisation von veranstaltungsbegleitenden Materialien, dennoch wissen wir nicht, ob die vorliegenden Dokumente
				auf dem aktuellen Stand sind oder ob neue Materialien für einige Lehrveranstaltungen existieren.<br>
				Sollten wir Lehrveranstaltungen übersehen haben, welche für die kommende Vorlesungszeit relevant sind und hierfür Materialien zur Verfügung stehen, so teilt uns dies bitte mit.<br>
				<br>
				Sebastian Mende<br>
				Skriptverantwortlicher<br>
				</p>
			</div>
			";
?>