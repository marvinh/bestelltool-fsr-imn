<?php
	if ($session_valid == true)
	{
		//Status der Bestell-Sperre abfragen
		$query = "
		SELECT ID
		FROM Status
		WHERE ID='1'
		LIMIT 1";
		$result = $connection->query($query);
		if ($row = $result->fetch_assoc())
		{
			//Bestellungsannahme ist gesperrt
			$dblock = true;
		}
		else
		{
			//Bestellungsannahme ist freigegeben
			$dblock = false;
		}
		
		//Aktionen des Nutzers verarbeiten
		if (isset($_POST['send0']))
		{
			//Der Button zum Übernehmen des Druckstatus wurde gedrückt
			$query = "
			SELECT SBID, SID ,COUNT(SID) AS Anzahl
			FROM SkriptBestellung
			WHERE gedruckt = 0 ";
			if ($script_ack == true)
			{
				$query = $query."AND Bestaetigt = 1";
			}
			$query = $query."
			GROUP BY SID
			";
						
			$error = false;
			$sid = array();
			$sid_count = array();
					
			//zunächst werden die übertragenen Felder geprüft
			$result=$connection->query($query);
			while ($row = $result->fetch_assoc())
			{						
				$sbid = $row['SBID'];
				if (isset($_POST[$sbid]))
				{
					//die übertragenen SBIDs sind gültig, entferne nun eventuelle Tags aus den
					//übertragenen Feldern
					$_POST[$sbid] = mysqli_real_escape_string($connection,$_POST[$sbid]);
					$_POST[$sbid] = str_replace('"','',$_POST[$sbid]);
					$_POST[$sbid] = strip_tags($_POST[$sbid]);
					if ($_POST[$sbid] == "")
					{
						$_POST[$sbid] = 0;
					}
							
					//übernimm die SID in das Array
					$sid[] = $row['SID'];
					$sid_count[$row['SID']] = $_POST[$sbid];
				}
				else
				{
					//es wurde eine fehlerhafte ID übertragen -> Eingabe wird verworfen
					$error = true;
				}
			}
					
			if ($error == false)
			{
				//alle übertragenen SBIDs sind gültig, ermittle nun alle Datensätze, bei denen die SID mit derjenigen
				//übereinstimmt, die zur SBID gehört, sortiere diese so, dass die ältesten Bestellungen als erstes
				//angezeigt werden und begrenze die Menge der Datensätze auf die Menge an Skripten, die gedruckt wurden.
				foreach ($sid as $i => $value)
				{
					$query = "
					SELECT SBID, UNIX_TIMESTAMP(Bestelldatum) AS Datum
					FROM SkriptBestellung
					WHERE SID = ".$value."
					AND gedruckt = 0 ";
					if ($script_ack == true)
					{
						$query = $query."AND Bestaetigt = 1";
					}
					$query = $query."
					ORDER BY Datum ASC
					LIMIT ".$sid_count[$value]."
					";
							
					$result = $connection->query($query);
					while ($row = $result->fetch_assoc())
					{
						//Nun setze alle zurückgegebenen Datensätze das Feld "gedruckt" auf 1 und aktualisiere das Feld "Druckdatum"
						$query = "
						UPDATE SkriptBestellung
						SET gedruckt = 1, druckdatum = NOW()
						WHERE SBID = ".$row['SBID']."";
							
						$result2 = $connection2->query($query);
					}
				}
			}					
		}
		if (isset($_POST['send1']))
		{
			//Der Button zum Übernehmen des Abholstatus wurde gedrückt
			if (isset($_POST['catch']))
			{
				//BID und SID zu jeder übergebenen SBID ermitteln
				$sbid_list = implode(',',$_POST['catch']);
						
				$sbid_list = mysqli_real_escape_string($connection,$sbid_list);
				$sbid_list = str_replace("'",'',$sbid_list);
				$sbid_list = strip_tags($sbid_list);
						
				$query = "
				SELECT SBID, BID, SID
				FROM skriptbestellung
				WHERE SBID IN (".$sbid_list.")
				AND gedruckt = '1'";
														
				$result = $connection->query($query);
				while ($row = $result->fetch_assoc())
				{
					if (isset($_POST[$row['SBID']]))
					{
						$_POST[$row['SBID']] = mysqli_real_escape_string($connection,$_POST[$row['SBID']]);
						$_POST[$row['SBID']] = str_replace("'",'',$_POST[$row['SBID']]);
						$_POST[$row['SBID']] = strip_tags($_POST[$row['SBID']]);
																
						$query = "
						DELETE FROM skriptbestellung
						WHERE SID = '".$row['SID']."'
						AND BID = '".$row['BID']."'
						AND gedruckt = '1' 
						ORDER BY UNIX_TIMESTAMP(Druckdatum) ASC
						LIMIT ".$_POST[$row['SBID']]."";
								
						$result2 = $connection2->query($query);
					}
				}
			}
		}
		if (isset($_POST['send2']))
		{
			if ($dblock == false)
			{
				//Bestellungsannahme ist freigegeben -> Flag "order_lock" setzen
				$query = "
				INSERT INTO Status (ID, Flag)
				VALUES (1, 'order_lock')";
				
				$result = $connection->query($query);
				$dblock = true;
			}
			else
			{
				//Bestellungsannahme ist gesperrt -> Flag "order_lock" löschen
				$query = "
				DELETE FROM Status
				WHERE ID='1'";
				
				$result = $connection->query($query);
				$dblock = false;
			}
		}
		//Anzeige der bestellten Skripte, die noch nicht gedruckt wurden
		echo "
				<form action=\"".$_SERVER["PHP_SELF"]."?page=overview\" method=\"post\" accept-charset=\"UTF-8\">";
		
		if ($dblock == false)
		{
			//Bestellungsannahme ist freigegeben
			echo "<input type=\"submit\" name=\"send2\" value=\"Bestellungsannahme ist freigegeben\">";
		}
		else
		{
			//Bestellungsannahme ist gesperrt
			echo "<input type=\"submit\" name=\"send2\" value=\"Bestellungsannahme ist gesperrt\">";
		}
					
		echo "</form>
				<h2>Angenommene Skriptbestellungen</h2>
				<p>Hier werden alle bestellten Skripte angezeigt,";
		if ($script_ack == true)
		{
			echo " die vom Besteller bestätigt wurden und";
		}
		echo " die noch nicht gedruckt wurden.</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=overview\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"text-align: center\" border=\"1\" rules=\"groups\">
					<colgroup>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<thead>
					<tr>
						<th>&#160;Modulbezeichnung&#160;</th>
						<th>&#160;Dozent&#160;</th>
						<th>&#160;Preis&#160;</th>
						<th>&#160;Anzahl&#160;</th>
						<th></th>
					</tr>
					</thead>
					<tbody>";
									
		$query = "
		SELECT SBID, Modulbezeichnung, CONCAT(FORMAT(Skript.Preis,2,'de_DE'),' €') AS Preis,
		Name , COUNT(Skriptbestellung.SID) AS Anzahl
		FROM Skriptbestellung INNER JOIN
		(Skript INNER JOIN Dozent
		ON Dozent.DID = Skript.DID)
		ON Skriptbestellung.SID = Skript.SID
		WHERE gedruckt = 0 ";
		if ($script_ack == true)
		{
			$query = $query."AND Bestaetigt = 1 ";
		}
		$query = $query."GROUP BY Skriptbestellung.SID";
										
		$result=$connection->query($query);
		$index = 1;
		while ($row = $result->fetch_assoc())
		{
			echo "
					<tr>
						<td>&#160;".$row['Modulbezeichnung']."&#160;</td>
						<td>&#160;".$row['Name']."&#160;</td>
						<td>&#160;".$row['Preis']."&#160;</td>
						<td>&#160;".$row['Anzahl']."&#160;</td>
						<td>&#160;<input name=\"".$row['SBID']."\" type=\"text\" size=\"1\" maxlength=\"10\" tabindex=\"".$index."\">&#160;gedruckt&#160;</td>
					</tr>";
			$index = $index++;
		}
		echo "
					<tr>
						<td colspan=\"5\" style=\"text-align: right\"><input name=\"send0\" type=\"submit\" value=\"Druckstatus übernehmen\"></td>
					</tr>
				</table>
				</form>
				<h2 style=\"margin-top: 2em\">Gedruckte Skripte</h2>
				<p>Hier werden alle gedruckten Skripte angezeigt, ".
				"die noch nicht abgeholt wurden.
				</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=overview\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"text-align: center\" border=\"1\" rules=\"groups\">
					<colgroup>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<thead>
					<tr>
						<th colspan=\"2\">&#160;Besteller&#160;</th>
						<th>&#160;Modulbezeichnung&#160;</th>
						<th>&#160;Dozent&#160;</th>
						<th>&#160;Anzahl&#160;</th>
						<th>&#160;Einzelpreis&#160;</th>
						<th>&#160;Gesamtpreis&#160;</th>
						<th></th>
					</tr>
					</thead>
					<tbody>";
					
		$query = "
		SELECT Nachname, Vorname, Modulbezeichnung, Name, SBID,
		CONCAT(FORMAT(Skript.Preis,2,'de_DE'),' €') AS Preis,
		COUNT(SkriptBestellung.SID) AS Anzahl, CONCAT(FORMAT(ROUND(SUM(Skript.Preis),2),2,'de_DE'),' €') AS Gesamtpreis
		FROM Besteller INNER JOIN (
		SkriptBestellung INNER JOIN (
		Skript INNER JOIN Dozent 
		ON Skript.DID = Dozent.DID) 
		ON SkriptBestellung.SID = Skript.SID)
		ON Besteller.BID = SkriptBestellung.BID
		WHERE gedruckt = 1 ";
		if ($script_ack == true)
		{
			$query = $query."AND Bestaetigt = 1 ";
		}
		$query = $query."
		GROUP BY Nachname, Vorname, SkriptBestellung.SID
		ORDER BY Nachname, Vorname ASC";
					
		$result = $connection->query($query);
		$bn = "";
		while ($row = $result->fetch_assoc())
		{
			if ($bn != $row['Vorname']." ".$row['Nachname'])
			{
				$bn = $row['Vorname']." ".$row['Nachname'];
				$s = " style=\"border-style: dotted none none none; border-color: black; border-width: 1px\"";
			}
			else
			{
				$row['Vorname'] = "";
				$row['Nachname'] = "";
				$s = "";
			}
			echo "
					<tr".$s.">
						<td style=\"text-align: right\">&#160;".$row['Vorname']."&#160;</td>
						<td style=\"text-align: left\">".$row['Nachname']."&#160;</td>
						<td>&#160;".$row['Modulbezeichnung']."&#160;</td>
						<td>&#160;".$row['Name']."&#160;</td>
						<td>&#160;".$row['Anzahl']."&#160;</td>
						<td>&#160;".$row['Preis']."&#160;</td>
						<td>&#160;".$row['Gesamtpreis']."&#160;</td>
						<td class=\"nb\"><input type=\"checkbox\" name=\"catch[]\" value=\"".$row['SBID']."\" tabindex=\"$index\">".
						"<input type=\"text\" name=\"".$row['SBID']."\" size=\"1\" maxlength=\"10\" value=\"".$row['Anzahl']."\">&#160;abgeholt&#160;</td>
					</tr>";
			$index = $index++;
		}
					
		echo "
					<tr>
						<td colspan=\"9\" style=\"text-align: right\"><input name=\"send1\" type=\"submit\" value=\"Abholstatus übernehmen\"></td>
					</tr>
					</tbody>
				</table>
				</form>";
		}
?>