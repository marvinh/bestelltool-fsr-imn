<?php
	if ($session_valid == true)
	{
		echo "
				<h2>Passwort ändern</h2>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=password\" method=\"post\" accept-charset=\"UTF-8\">
				<table>
					<colgroup>
						<col>
						<col>
						<col width=\"100%\">
					</colgroup>
					<tr>
						<td>Altes&#160;Passwort:&#160;&#160;</td>
						<td><input type=\"password\" name=\"old\" size=\"30\" maxlength=\"50\" tabindex=\"2\"></td>
					</tr>
					<tr>
						<td>Neues&#160;Passwort:&#160;&#160;</td>
						<td><input type=\"password\" name=\"password\" size=\"30\" maxlength=\"50\" tabindex=\"2\"></td>
					</tr>
					<tr>
						<td>Passwort&#160;wiederholen:&#160;</td>
						<td><input type=\"password\" name=\"password2\" size=\"30\" maxlength=\"50\" tabindex=\"2\"></td>
					</tr>
					<tr>
						<td colspan=\"2\">
						<p style=\"text-align: justify\">
						Hinweis: Wenn Sie das Passwort-Feld leer lassen und auf \"Passwort übernehmen\" drücken, 
						wird ihr Passwort gelöscht.<br>";
						
		if ($deny_empty == false)
			echo "Sie können sich dann ohne Passwort einloggen.";
		else
			echo 	"Sie können sich dann nicht mehr einloggen, bis ".
					"ein berechtigter Benutzer ein neues Passwort für Sie ".
					"gesetzt hat.";
						
		echo "</p>
						</td>
					</tr>
					<tr>
						<td colspan=\"2\"><input type=\"submit\" name=\"chpw\" value=\"Passwort übernehmen\" style=\"width: 100%\"></td>
					</tr>";
														
		if (isset($pw_ch))
		{
			if ($pw_ch == false)
			{
				echo "
					<tr>
						<td colspan=\"2\" class=\"error\">Passwortänderung fehlgeschlagen.</td>
					</tr>";
				if (isset($not_identic) AND $not_identic == true)
				{
					echo "
					<tr>
						<td colspan=\"2\" class=\"error\">Sie haben sich bei der Eingabe Ihres neuen Passwortes vertippt. Bitte geben Sie es noch mal ein.</td>
					</tr>";
				}
				else
				{
					if (isset($invalid_pw) AND $invalid_pw == true)
					{
						echo "
					<tr>
						<td colspan=\"2\" class=\"error\">Das von Ihnen eingegebene alte Passwort stimmt nicht mit dem in der Datenbank hinterlegten Passwort überein.</td>
					</tr>";
					}
				}
			}
			else
			{
				echo "
					<tr>
						<td colspan=\"2\" style=\"font-weight: bold\">Passwortänderung erfolgreich.</td>
					</tr>";
			}
		}			
		echo "
				</table>
				</form>";
	}
?>