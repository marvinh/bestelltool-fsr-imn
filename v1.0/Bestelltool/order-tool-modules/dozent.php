<?php
	if ($session_valid == true)
	{
		$fehler = false;
		$fehler2 = array();
		$correctfields = 0;
		if (isset($_POST['send0']))
		{
			//Skripte sollen bearbeitet oder gelöscht werden
							
			if (isset($_POST['name']))
			{
				$names = implode('±',$_POST['name']);
				$names = mysqli_real_escape_string($connection, $names);
				$names = str_replace("'",'',$names);
				$names = strip_tags($names);
				$names = explode('±',$names);
								
				$correctfields++;
			}
			if (isset($_POST['did']))
			{
				$dz = implode('±',$_POST['did']);
				$dz = mysqli_real_escape_string($connection, $dz);
				$dz = str_replace("'",'',$dz);
				$dz = strip_tags($dz);
				$dz = explode('±',$dz);
								
				$correctfields++;
			}
			if (isset($_POST['update']))
			{
				$up = implode('±',$_POST['update']);
				$up = mysqli_real_escape_string($connection, $up);
				$up = str_replace("'",'',$up);
				$up = strip_tags($up);
				$up = explode('±',$up);
			}
			else
			{
				$up = array();
			}	
			
			if ($correctfields == 2)
			{
				$count = count($dz);
							
				for ($a = 0; $a < $count; $a++)
				{
					if (in_array($dz[$a], $up))
					{
						$pup = 1;
					}
					else
					{
						$pup = 0;
					}
									
					if (isset($_POST[$dz[$a]]))
					{
						if ($_POST[$dz[$a]] == '1')
						{
							//Dozent soll aktualisiert werden
							$query = "
							UPDATE Dozent
							SET Name='".$names[$a]."', pAktualisierung='".$pup."'
							WHERE DID='".$dz[$a]."'";
											
							$result = $connection->query($query);
						}
						elseif ($_POST[$dz[$a]] == '2')
						{
							//Dozent soll gelöscht werden
											
							//Prüfen, ob dem Dozent noch ein Skript zugeordnet ist
							$query = "
							SELECT DID FROM Skript
							WHERE DID='".$dz[$a]."'
							LIMIT 1";
											
							$result = $connection->query($query);
							if ($row = $result->fetch_assoc())
							{
								//Dem Dozent ist noch ein Skript zugeordnet -> zeige Fehlermeldung an
								$fehler = true;
								$fehler2[] = $dz[$a];
							}
							else
							{
								//Dem Dozent ist kein Skript zugeordnet -> lösche den Dozent
								$query = "
								DELETE FROM Dozent
								WHERE DID='".$dz[$a]."'";
												
								$result = $connection->query($query);
							}
						}
					}
				}
			}
		}
		if (isset($_POST['send1']))
		{
			//Skripte sollen hinzugefügt werden
			if (isset($_POST['name']))
			{
				$names = implode('±',$_POST['name']);
				$names = mysqli_real_escape_string($connection, $names);
				$names = str_replace("'",'',$names);
				$names = strip_tags($names);
				$names = explode('±',$names);
								
				$correctfields++;
			}
			if (isset($_POST['update']))
			{
				$up = implode('±',$_POST['update']);
				$up = mysqli_real_escape_string($connection, $up);
				$up = str_replace("'",'',$up);
				$up = strip_tags($up);
				$up = explode('±',$up);
			}
			else
			{
				$up = array();
			}
			if ($correctfields == 1)
			{
				$count = count($names);
				for ($a = 0; $a < $count; $a++)
				{
					if (trim($names[$a]) != "")
					{
						if (in_array($a, $up))
						{
							$pup = 1;
						}
						else
						{
							$pup = 0;
						}
									
						$query = "
						INSERT INTO Dozent (Name, pAktualisierung)
						VALUES ('".$names[$a]."','".$pup."')";
									
						$result = $connection->query($query);
					}
				}
			}
		}
		echo "
				<h2>Dozenten bearbeiten</h2>
				<p>Hier können Sie die im System hinterlegten Dozenten bearbeiten</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=dozent\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black\">
					<colgroup>
						<col>
						<col>
						<col>
					</colgroup>
					<tr>
						<th>&#160;Name des Dozenten&#160;</th>
						<th>&#160;Partielle Aktualisierung&#160;</th>
						<th>&#160;Aktion&#160;</th>
					</tr>";
				
		$query = "
		SELECT DID, Name, pAktualisierung
		FROM Dozent
		ORDER BY Name ASC";
					
		$result = $connection->query($query);
					
		$index = 1;
					
		while ($row = $result->fetch_assoc())
		{
			echo "
					<tr>
						<td>&#160;<input name=\"name[]\" type=\"text\" size=\"25\" value=\"".$row['Name']."\" tabindex=\"".$index."\" required>&#160;</td>
						<td>
							<input name=\"update[]\" type=\"checkbox\" value=\"".$row['DID']."\" ";
								
			if ($row['pAktualisierung'] == 1)
			{
				echo "checked ";
			}
								
			echo "tabindex=\"".($index+1)."\">Skripte werden partiell aktualisiert&#160;
							<input type=\"hidden\" name=\"did[]\" value=\"".$row['DID']."\">&#160;
						</td>
						<td>&#160;<input name=\"".$row['DID']."\" type=\"radio\" value=\"1\" tabindex=\"".($index+2)."\" checked>übernehmen<input name=\"".$row['DID']."\" type=\"radio\" value=\"2\" tabindex=\"".($index+3)."\">löschen&#160;";
								
			if (in_array($row['DID'], $fehler2))
			{
				echo "<span class=\"error\">*</span>";
			}
								
			echo "</td>
					</tr>";
			$index = $index+4;
		}
											
		echo "
					<tr style=\"text-align: right\"><td colspan=\"3\"><input name=\"send0\" type=\"submit\" value=\"Anwenden\"></td></tr>
				</table>
				</form>";
						
		if ($fehler == true)
		{
			echo "
				<p class=\"error\" style=\"margin: 0px\">Fehler: Einige Dozenten konnten nicht gelöscht werden, da ihnen noch Skripte zugeordnet sind</p>";
		}
						
		echo "
				<h2 style=\"margin-top: 2em\">Dozenten bearbeiten</h2>
				<p>Hier können Sie die im System hinterlegten Dozenten bearbeiten</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=dozent\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black\">
					<colgroup>
						<col>
						<col>
					</colgroup>
					<tr>
						<th>&#160;Name des Dozenten&#160;</th>
						<th>&#160;Partielle Aktualisierung&#160;</th>
					</tr>";
						
		for ($a = 0; $a < 5; $a++)
		{
			echo "
					<tr>
						<td>&#160;<input name=\"name[]\" type=\"text\" size=\"25\" value=\"\" tabindex=\"".$index."\">&#160;</td>
						<td><input name=\"update[]\" type=\"checkbox\" value=\"".$a."\" tabindex=\"".($index+1)."\">Skripte werden partiell aktualisiert&#160;</td>
					</tr>";
			$index = $index+2;
		}
							
		echo "
					<tr style=\"text-align: right\"><td colspan=\"2\"><input name=\"send1\" type=\"submit\" value=\"Dozenten hinzufügen\"></td></tr>
				</table>
				</form>";
	}
?>