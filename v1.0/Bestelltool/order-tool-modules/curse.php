<?php
	if ($session_valid == true)
	{
		$correctfields = 0;
		if (isset($_POST['send0']))
		{
			//Studiengänge sollen bearbeitet oder gelöscht werden
							
			if (isset($_POST['curse']))
			{
				$curses = implode('±',$_POST['curse']);
				$curses = mysqli_real_escape_string($connection, $curses);
				$curses = str_replace("'",'',$curses);
				$curses = strip_tags($curses);
				$curses = explode('±',$curses);
								
				$correctfields++;
			}
			if (isset($_POST['stid']))
			{
				$stid = implode('±',$_POST['stid']);
				$stid = mysqli_real_escape_string($connection, $stid);
				$stid = str_replace("'",'',$stid);
				$stid = strip_tags($stid);
				$stid = explode('±',$stid);
								
				$correctfields++;
			}
			if ($correctfields == 2)
			{
				$count = count($stid);
				if (count($curses == $count))
				{
					for ($a = 0; $a < $count; $a++)
					{
						if (isset($_POST[$stid[$a]]))
						{
							if ($_POST[$stid[$a]] == '1' AND trim($curses[$a]) != "")
							{
								//Änderungen an Studiengang soll übernommen werden
								$query = "
								UPDATE Studiengang
								SET Bezeichnung='".$curses[$a]."'
								WHERE StID='".$stid[$a]."'";
										
								$result = $connection->query($query);
												
								//Verknüpfungen in Tabelle "StudiengangSkript", die mit dem aktuellen Studiengang verknüpft sind, löschen
								$query = "
								DELETE FROM StudiengangSkript
								WHERE StID='".$stid[$a]."'";
												
								$result = $connection->query($query);
												
								//Falls Skripte aus der Liste ausgewählt wurden, Verknüpfungen in Tabelle "StudiengangSkript" neu erstellen
								if (isset($_POST['sid'.$stid[$a]]))
								{
									$script = implode('±',$_POST['sid'.$stid[$a]]);
									$script = mysqli_real_escape_string($connection, $script);
									$script = str_replace("'",'',$script);
									$script = strip_tags($script);
									$script = explode('±',$script);
													
									foreach($script as $value)
									{
										$query = "
										INSERT INTO StudiengangSkript (SID, StID)
										VALUES ('".$value."', '".$stid[$a]."')";
														
										$result = $connection->query($query);
									}
								}
							}
							elseif ($_POST[$stid[$a]] == '2')
							{
								//Studiengang soll gelöscht werden
								$query = "
								DELETE FROM Studiengang
								WHERE StID='".$stid[$a]."'";
												
								$result = $connection->query($query);
												
								$query = "
								DELETE FROM StudiengangSkript
								WHERE StID='".$stid[$a]."'";
												
								$result = $connection->query($query);
							}
						}
					}
				}
			}
		}
		if (isset($_POST['send1']))
		{
			//Ein Skript soll hinzugefügt werden
			if (isset($_POST['curse']))
			{
				$curses = implode('±',$_POST['curse']);
				$curses = mysqli_real_escape_string($connection, $curses);
				$curses = str_replace("'",'',$curses);
				$curses = strip_tags($curses);
				$curses = explode('±',$curses);
								
				$correctfields++;
			}
			if ($correctfields == 1)
			{
				$count = count($curses);
								
				for ($a = 0; $a < 1; $a++)
				{
					if (trim($curses[$a]) != "")
					{
						//Studiengang hinzufügen
						$query = "INSERT INTO Studiengang (Bezeichnung) VALUES ('".$curses[$a]."')";
										
						$result = $connection->query($query);
										
						if (isset($_POST[$a]))
						{
							$sid = implode('±',$_POST[$a]);
							$sid = mysqli_real_escape_string($connection, $sid);
							$sid = str_replace("'",'',$sid);
							$sid = strip_tags($sid);
							$sid = explode('±',$sid);
								
							$query = "
							SELECT StID
							FROM Studiengang
							WHERE Bezeichnung='".$curses[$a]."'
							LIMIT 1";
											
							$result = $connection->query($query);
							if ($row = $result->fetch_assoc())
							{
								//ID des neu eingefügten Skripts wurde aus der Datenbank gelesen
								$stid = $row['StID'];
												
								foreach($sid as $value)												
								{
									$query = "
									INSERT INTO StudiengangSkript (SID, StID)
									VALUES ('".$value."','".$stid."')";
												
									$result = $connection->query($query);
								}
							}
						}
					}
				}
			}
		}
		echo "
				<h2>Studiengänge bearbeiten</h2>
				<p>Hier können Sie die im System hinterlegten Studiengänge bearbeiten.<br>
				Die Listen in der Spalte \"Skripte\" erlauben die Auswahl mehrerer Einträge<br>(halten Sie STRG gedrückt, während Sie einen Eintrag anklicken).</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=curse\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black\">
					<colgroup>
						<col>
						<col>
					</colgroup>
					<tr>
						<th>&#160;Bezeichnung&#160;</th>
						<th>&#160;Skripte&#160;</th>
					</tr>";
								
		//Skripte cachen
		$query = "
		SELECT SID, Modulbezeichnung
		FROM Skript
		ORDER BY Modulbezeichnung ASC";
						
		$result = $connection->query($query);
						
		$scripts = array();
						
		while ($row = $result->fetch_assoc())
		{
			$scripts[$row['SID']] = $row['Modulbezeichnung'];
		}
						
		//Tabelle "StudiengangSkript" cachen
		$query = "
		SELECT StID, SID
		FROM StudiengangSkript";
						
		$result = $connection->query($query);
						
		$selection = array();
						
		while ($row = $result->fetch_assoc())
		{
			if (isset($selection[$row['StID']]))
			{
				$selection[$row['StID']] = $selection[$row['StID']]."-".$row['SID']."-";
			}
			else
			{
				$selection[$row['StID']] = "-".$row['SID']."-";
			}
		}
												
		//Studiengänge anzeigen
		$query = "
		SELECT StID, Bezeichnung
		FROM Studiengang
		ORDER BY Bezeichnung ASC";
						
		$result = $connection->query($query);
						
		$index = 1;
						
		while ($row = $result->fetch_assoc())
		{
			echo "
					<tr>
						<td style=\"vertical-align: top\">
							&#160;<input name=\"curse[]\" type=\"text\" size=\"25\" value=\"".$row['Bezeichnung']."\" tabindex=\"".$index."\" required>&#160;<br>
							&#160;<i><u>Aktion:</u></i><br>
							&#160;<input name=\"".$row['StID']."\" type=\"radio\" value=\"1\" tabindex=\"".($index+2)."\" checked>übernehmen&#160;<br>
							&#160;<input name=\"".$row['StID']."\" type=\"radio\" value=\"2\" tabindex=\"".($index+3)."\">löschen&#160;
							&#160;<input type=\"hidden\" name=\"stid[]\" value=\"".$row['StID']."\">&#160;
						</td>
						<td>
							<select name=\"sid".$row['StID']."[]\" size=\"5\" tabindex=\"".($index+1)."\" multiple>";
									
			foreach($scripts as $key => $value)
			{
				//Auswahlliste anzeigen
				if (strpos($selection[$row['StID']], "-".$key."-") === false)
				{
					$selected = "";
				}
				else
				{
					$selected = " selected";
				}
											
				echo "
								<option value=\"".$key."\"".$selected.">".$value."</option>";
			}
										
			echo "
							</select>
						</td>
					</tr>";
							
			$index = $index+4;
		}
							
		echo "
					<tr style=\"text-align: right\">
						<td colspan=\"2\"><input name=\"send0\" type=\"submit\" value=\"Anwenden\"></td>
					</tr>
				</table>
				</form>
				<h2 style=\"margin-top: 2em\">Studiengang hinzufügen</h2>
				<p>Hier können Sie einen Studiengang hinzufügen.<br>
				Die Listen in der Spalte \"Skripte\" erlauben die Auswahl mehrerer Einträge<br>(halten Sie STRG gedrückt, während Sie einen Eintrag anklicken).</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=curse\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black\">
					<colgroup>
						<col>
						<col>
					</colgroup>
					<tr>
						<th>&#160;Bezeichnung&#160;</th>
						<th>&#160;Skripte&#160;</th>
					</tr>";
						
		for ($a = 0; $a < 1; $a++)
		{
			echo "
					<tr>
						<td style=\"vertical-align: top\">&#160;<input name=\"curse[]\" type=\"text\" size=\"25\" value=\"\" tabindex=\"".$index."\" required>&#160;</td>
						<td>
							<select name=\"".$a."[]\" size=\"5\" tabindex=\"".($index+1)."\" multiple>";
							
			foreach($scripts as $key => $value)
			{
				//Auswahlliste anzeigen
									
				echo "
								<option value=\"".$key."\"".$selected.">".$value."</option>";
			}
									
			echo "
							</select>
						</td>
					</tr>";
							
			$index = $index+2;
		}
						
		echo "
					<tr style=\"text-align: right\">
						<td colspan=\"2\"><input name=\"send1\" type=\"submit\" value=\"Studiengang hinzufügen\"></td>
					</tr>
				</table>
				</form>";
	}
?>