<?php
	if (isset($_GET['bid']) AND isset($_GET['code']))
	{
		echo "<div class=\"order_page\">";
		
		$_GET['bid'] = mysqli_real_escape_string($connection, $_GET['bid']);
		$_GET['code'] = mysqli_real_escape_string($connection, $_GET['code']);
		
		//first check if the BID and the ACK-Code are valid
		$query = "SELECT bid,ackcode,bestaetigt
		FROM skriptbestellung
		WHERE bid=".$_GET['bid']."
		AND ackcode=".$_GET['code']."
		AND bestaetigt=0";
			
		$result = $connection->query($query);
		if ($row = $result->fetch_assoc())
		{
			//If the specified BID and code are valid and the
			//order is not acknowledged yet, set it to acknoledged
			$query = "UPDATE skriptbestellung
			SET bestaetigt=1
			WHERE bid=".$_GET['bid']."
			AND ackcode=".$_GET['code'];
				
			$result = $connection->query($query);
			if ($result)
			{
				echo "<p>Ihre Skriptbestellung wurde bestätigt und wird in den nächsten Tagen
				gedruckt. Bitte prüfen Sie regelmäßig den <a href=\"".$_SERVER['SCRIPT_NAME']."?page=status\">Status</a>
				Ihrer Skriptbestellung und holen diese innerhalb von 14 Tagen nach dem Druck ab.</p>";
			}
			else
			{
				echo "<p class=\"error\">Fehler: Bei der Bestätigung der Skriptbestellung ist ein Fehler aufgetreten. Bitte versuchen Sie es später erneut.</p>";
			}
		}
		else
		{
			echo "<p class=\"error\">Fehler: Die Skriptbestellung konnte nicht bestätigt werden, 
			da die übergebenen Parameter ungültig sind.</p>";
		}
		echo "</div>";
	}
?>