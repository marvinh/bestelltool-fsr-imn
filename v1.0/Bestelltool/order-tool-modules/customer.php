<?php
	if ($session_valid == true)
	{
		echo "
				<h2>Besteller</h2>
				<p>Hier wird eine Übersicht aller Besteller angezeigt.</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=customer\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black\">
					<colgroup>
						<col>
						<col>
					</colgroup>
					<tr>
						<th>&#160;Name&#160;</th>
						<th>&#160;E-Mail&#160;</th>
					</tr>";
						
		$query = "
		SELECT Nachname, Vorname, Mail
		FROM Besteller
		ORDER BY Nachname ASC";
						
		$result = $connection->query($query);
						
		while ($row = $result->fetch_assoc())
		{
			echo "
					<tr>
						<td>&#160;".$row['Vorname']." ".$row['Nachname']."&#160;</td>
						<td>&#160;".$row['Mail']."&#160;</td>
					</tr>";
		}
							
		echo "
				</table>
				</form>";
	}
?>