<?php
	if ($session_valid == true)
	{
		$fehler = false;
		$fehler2 = false;
		$fehler3 = array();
		$all_scripts = true;
						
		if (isset($_POST['send0']))
		{
			//Skripte sollen gelöscht oder aktualisiert werden
			$correctfields = 0;
			if (isset($_POST['script']))
			{
				$scripts = implode('±',$_POST['script']);
				$scripts = mysqli_real_escape_string($connection, $scripts);
				$scripts = str_replace("'",'',$scripts);
				$scripts = strip_tags($scripts);
				$scripts = explode('±',$scripts);
								
				$correctfields++;
			}
			if (isset($_POST['price']))
			{
				$price = implode('±',$_POST['price']);
				$price = mysqli_real_escape_string($connection, $price);
				$price = str_replace("'",'',$price);
				$price = strip_tags($price);
				$price = explode('±',$price);
								
				$correctfields++;
			}
			if (isset($_POST['semester']))
			{
				$semester = implode('±',$_POST['semester']);
				$semester = mysqli_real_escape_string($connection, $semester);
				$semester = str_replace("'",'',$semester);
				$semester = strip_tags($semester);
				$semester = explode('±',$semester);
							
				$correctfields++;
			}
			if (isset($_POST['did']))
			{
				$dz = implode('±',$_POST['did']);
				$dz = mysqli_real_escape_string($connection, $dz);
				$dz = str_replace("'",'',$dz);
				$dz = strip_tags($dz);
				$dz = explode('±',$dz);
								
				$correctfields++;
			}
			if (isset($_POST['sid']))
			{
				$idlist = implode(',',$_POST['sid']);
				$idlist = mysqli_real_escape_string($connection, $idlist);
				$idlist = str_replace("'",'',$idlist);
				$idlist = strip_tags($idlist);
								
				$ids = explode(',',$idlist);
								
				$correctfields++;
			}
			if ($correctfields == 5)
			{
				//Alle notwendigen Felder wurden übermittelt, Verarbeitung beginnt
																
				$amount = count($ids);
								
				for ($index = 0; $index < $amount; $index++)
				{
					if (isset($_POST[$ids[$index]]) == false)
					{
						$fehler = true;
						break;
					}
																		
					if ($_POST[$ids[$index]] == "1")
					{
						//Die Änderungen an dem aktuellen Skript sollen übernommen werden
						if (isset($_POST['summer']))
						{
							if (in_array($ids[$index], $_POST['summer']))
							{
								$summer = '1';
							}
							else
							{
								$summer = '0';
							}
						}
						else
						{
							$summer = '0';
						}
										
						if (isset($_POST['winter']))
						{
							if (in_array($ids[$index], $_POST['winter']))
							{
								$winter = '1';
							}
							else
							{
								$winter = '0';
							}
						}
						else
						{
							$winter = '0';
						}
										
						if (isset($_POST['avail']))
						{
							if (in_array($ids[$index], $_POST['avail']))
							{
								$avail = '1';
							}
							else
							{
								$avail = '0';
							}
						}
						else
						{
							$avail = '0';
						}
										
						$query = "
						SELECT DID FROM Dozent WHERE DID='".$dz[$index]."'
						LIMIT 1";

						$result = $connection->query($query);
						if ($row = $result->fetch_assoc())
						{
							$query = "
							UPDATE Skript
							SET Modulbezeichnung='".$scripts[$index]."',
							Preis='".str_replace(',','.',$price[$index])."',
							Semester='".$semester[$index]."',
							Sommersemester='".$summer."',
							Wintersemester='".$winter."',
							Verfuegbar='".$avail."',
							DID='".$dz[$index]."'
							WHERE SID='".$ids[$index]."'";
										
							$result = $connection->query($query);
						}										
					}
					elseif ($_POST[$ids[$index]] == "2")
					{
						//Das aktuelle Skript soll gelöscht werden
										
						//Prüfen, ob Skript noch mit Bestellungen verknüpft ist
						$query = "
						SELECT SID
						FROM Skriptbestellung
						WHERE SID='".$ids[$index]."'
						LIMIT 1";
										
						$result = $connection->query($query);
						if ($row = $result->fetch_assoc())
						{
							//Es ist noch mindestens eine Bestellung mit dem Skript verknüpft -> Fehlermeldung ausgeben
							$fehler2 = true; //zeigt an, dass mindestens 1 Skript nicht gelöscht werden konnte
							$fehler3[] = $ids[$index]; //aktuelle SkriptID zum Array hinzugefügen -> ermöglicht Darstellung der Skripte, die nicht gelöscht werden konnten
						}
						else
						{
							//Es sind keine Bestellungen mehr mit dem Skript verknüpft -> Skript löschen
											
							$query = "
							DELETE FROM StudiengangSkript
							WHERE SID='".$ids[$index]."'";
											
							$result = $connection->query($query);
																						
							$query = "
							DELETE FROM Skript
							WHERE SID='".$ids[$index]."'";
											
							$result = $connection->query($query);
						}
					}
				}
			}
		}
		if (isset($_POST['send1']))
		{
			//Skripte sollen hinzugefügt werden
			$correctfields = 0;
							
			if (isset($_POST['script']))
			{
				$scripts = implode('±',$_POST['script']);
				$scripts = mysqli_real_escape_string($connection, $scripts);
				$scripts = str_replace("'",'',$scripts);
				$scripts = strip_tags($scripts);
				$scripts = explode('±',$scripts);
								
				$correctfields++;
			}
			if (isset($_POST['price']))
			{
				$price = implode('±',$_POST['price']);
				$price = mysqli_real_escape_string($connection, $price);
				$price = str_replace("'",'',$price);
				$price = strip_tags($price);
				$price = explode('±',$price);
								
				$correctfields++;
			}
			if (isset($_POST['semester']))
			{
				$semester = implode('±',$_POST['semester']);
				$semester = mysqli_real_escape_string($connection, $semester);
				$semester = str_replace("'",'',$semester);
				$semester = strip_tags($semester);
				$semester = explode('±',$semester);
								
				$correctfields++;
			}
			if (isset($_POST['did']))
			{
				$dz = implode('±',$_POST['did']);
				$dz = mysqli_real_escape_string($connection, $dz);
				$dz = str_replace("'",'',$dz);
				$dz = strip_tags($dz);
				$dz = explode('±',$dz);
								
				$correctfields++;
			}							
							
			if ($correctfields == 4)
			{
				$count1 = count($scripts);
				$count2 = count($price);
				$count3 = count($semester);
				$count3 = count($dz);
																
				if ($count1 == $count2 AND $count2 == $count3)
				{
					for ($a = 0; $a < $count1; $a++)
					{
						//Prüfen, ob in allen Zeilen, in denen mindestens eines der Eingabefelder Text enthält, die restlichen Eingabefelder ebenfalls Text enthalten.
						$line = 0;
						if (trim($scripts[$a]) != "")
						{
							$line++;
						}
						if (trim($price[$a]) != "")
						{
							$price[$a] = str_replace(',','.',$price[$a]);
							$line++;
						}
						if (trim($semester[$a]) != "")
						{
							$line++;
						}
						if (trim($dz[$a]) != "")
						{
							$query = "
							SELECT DID
							FROM Dozent
							WHERE DID = '".$dz[$a]."'
							LIMIT 1";
											
							$result = $connection->query($query);
							if ($row = $result->fetch_assoc())
							{
								$line++;
							}
						}
						if ($line == 4)
						{
							//Die aktuelle Zeile wurde vollständig ausgefüllt -> verarbeite Datensatz
							if (isset($_POST['summer']) AND in_array($a, $_POST['summer']))
							{
								$summer = 1;
							}
							else
							{
								$summer = 0;
							}
											
							if (isset($_POST['winter']) AND in_array($a, $_POST['winter']))
							{
								$winter = 1;
							}
							else
							{
								$winter = 0;
							}
											
							if (isset($_POST['avail']) AND in_array($a, $_POST['avail']))
							{
								$avail = 1;
							}
							else
							{
								$avail = 0;
							}
											
							$query = "
							INSERT INTO Skript (Modulbezeichnung, Preis, Semester, Sommersemester, Wintersemester, Verfuegbar, DID)
							VALUES ('".trim($scripts[$a])."','".trim($price[$a])."','".trim($semester[$a])."','".$summer."','".$winter."','".$avail."','".$dz[$a]."')";
											
							$result = $connection->query($query);
						}
					}
				}
			}
		}
		echo "
				<h2>Skripte bearbeiten</h2>
				<p>Hier können Sie die im System hinterlegten Skripte bearbeiten</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=script\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black\">
					<colgroup>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<tr>
						<th>&#160;Modulbezeichnung&#160;</th>
						<th>&#160;Preis&#160;</th>
						<th>&#160;Semester&#160;</th>
						<th>&#160;Verfügbarkeit&#160;</th>
						<th>&#160;Bestellbarkeit&#160;</th>
						<th>&#160;Dozent&#160;</th>
						<th>&#160;Aktion&#160;</th>
					</tr>";
					
		//Liste aller Dozenten aus der Datenbank laden
		$dozents = array();
						
		$query = "
		SELECT DID, Name
		FROM Dozent
		ORDER BY Name ASC";
						
		$result = $connection->query($query);
		while ($row = $result->fetch_assoc())
		{
			$dozents[$row['DID']] = $row['Name'];
		}
						
		//Liste aller Skripte aus der Datenbank laden
		$query = "
		SELECT SID, Modulbezeichnung, FORMAT(Skript.Preis,2,'de_DE') AS Preis, Semester, Sommersemester, Wintersemester, Verfuegbar, 
		Dozent.DID AS DID, Name
		FROM Skript INNER JOIN Dozent
		ON Dozent.DID = Skript.DID
		ORDER BY Modulbezeichnung";
							
		$result = $connection->query($query);
						
		$index = 1;
						
		while ($row = $result->fetch_assoc())
		{
			echo "
					<tr>
						<td style=\"text-align: center\">&#160;<input name=\"script[]\" type=\"text\" size=\"25\" value=\"".$row['Modulbezeichnung']."\" tabindex=\"".$index."\" required>&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"price[]\" type=\"text\" size=\"3\" value=\"".$row['Preis']."\" style=\"text-align: right\" tabindex=\"".($index+1)."\" required>&#160;€&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"semester[]\" type=\"text\" size=\"6\" value=\"".$row['Semester']."\" style=\"text-align: right\" tabindex=\"".($index+2)."\" required>&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"summer[]\" type=\"checkbox\" value=\"".$row['SID']."\" ";
			if ($row['Sommersemester'] == 1)
			{
				echo "checked";
			}
			echo " tabindex=\"".($index+3)."\">Sommersemester&#160;".
			"<input name=\"winter[]\" type=\"checkbox\" value=\"".$row['SID']."\" ";
			if ($row['Wintersemester'] == 1)
			{
				echo "checked";
			}
			echo " tabindex=\"".($index+4)."\">Wintersemester&#160;</td>
						<td style=\"text-align: center\"><input name=\"avail[]\" type=\"checkbox\" value=\"".$row['SID']."\"";
			if ($row['Verfuegbar'] == 1)
			{
				echo " checked";
			}
			echo " tabindex=\"".($index+5)."\">&#160;bestellbar&#160;</td>
						<td>
							<select name=\"did[]\" size=\"1\" tabindex=\"".($index+6)."\">";
			foreach($dozents as $id => $name)
			{
				echo "
								<option value=\"".$id."\"";
				if ($id == $row['DID'])
				{
					echo " selected";
				}
				echo ">".$name."</option>";
			}									
			echo "
							</select>
						</td>
						<td>
							&#160;&#160;<input name=\"".$row['SID']."\" type=\"radio\" value=\"1\" tabindex=\"".($index+7)."\" checked>übernehmen<input name=\"".$row['SID']."\" type=\"radio\" value=\"2\" tabindex=\"".($index+8)."\">löschen";

			if (in_array($row['SID'],$fehler3))
			{
				//Skript konnte nicht gelöscht werden->zeige es dem Nutzer durch ein angehängtes Sternchen an
				echo "<span class=\"error\">*</span>";
			}
								
			echo "
							<input type=\"hidden\" name=\"sid[]\" value=\"".$row['SID']."\">&#160;
						</td>
					</tr>";
			$index = $index+9;
		}
													
		echo "
					<tr style=\"text-align: right\">
						<td colspan=\"7\">";
							
		if ($fehler2 == true)
		{
			echo "<span class=\"error\">Fehler: Einige Skripte konnten nicht gelöscht werden, da sie noch mit Bestellungen verknüpft sind&#160;</span>";
		}
						
		echo "<input name=\"send0\" type=\"submit\" value=\"Anwenden\"></td>
					</tr>
				</table>
				</form>";
		if ($fehler == true)
		{
			echo "<p class=\"error\">Bei der Verarbeitung der Daten ist ein Fehler aufgetreten</p>";
		}
						
		echo "
				<h2 style=\"margin-top: 2em\">Skripte hinzufügen</h2>
				<p>Hier können Sie neue Skripte hinzufügen</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=script\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black\">
					<colgroup>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<tr>
						<th>&#160;Modulbezeichnung&#160;</th>
						<th>&#160;Preis&#160;</th>
						<th>&#160;Semester&#160;</th>
						<th>&#160;Verfügbarkeit&#160;</th>
						<th>&#160;Bestellbarkeit&#160;</th>
						<th>&#160;Dozent&#160;</th>
					</tr>";
		for ($a = 0; $a < 5; $a++)
		{
			echo "
					<tr>
						<td style=\"text-align: center\">&#160;<input name=\"script[]\" type=\"text\" size=\"25\" tabindex=\"".$index."\">&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"price[]\" type=\"text\" size=\"3\" style=\"text-align: right\" tabindex=\"".($index+1)."\">&#160;€&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"semester[]\" value=\"\" type=\"text\" size=\"6\" style=\"text-align: right\" tabindex=\"".($index+2)."\">&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"summer[]\" type=\"checkbox\" value=\"".$a."\" tabindex=\"".($index+3)."\">Sommersemester&#160;".
						"<input name=\"winter[]\" type=\"checkbox\" value=\"".$a."\" tabindex=\"".($index+4)."\">Wintersemester&#160;</td>
						<td style=\"text-align: center\"><input name=\"avail[]\" type=\"checkbox\" value=\"".$a."\" tabindex=\"".($index+5)."\">&#160;bestellbar&#160;</td>
						<td style=\"text-align: center\">
							<select name=\"did[]\" size=\"1\" tabindex=\"".($index+6)."\">";
									
			reset($dozents);
			echo "
								<option value=\"".key($dozents)."\" selected >".current($dozents)."</option>";
									
			$count = count($dozents);
									
			for($b = 1; $b < $count; $b++)
			{
				next($dozents);
				echo "
								<option value=\"".key($dozents)."\">".current($dozents)."</option>";
			}	

			$index = $index+6;
			echo "
							</select>
						</td>
					</tr>";
		}
							
		echo "
					<tr style=\"text-align: right\">
						<td colspan=\"6\"><input name=\"send1\" type=\"submit\" value=\"Skripte hinzufügen\"></td>
					</tr>
				</table>
				</form>";
	}
?>