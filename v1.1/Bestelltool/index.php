<?php
	//This is the order tool
	
	//******************************************************************************************************************************************
	//important variables which may be need to be reconfigured when migrating script
	//to other server
	$modules = "order-tool-modules";	//folder containing the modules of the order tool
	$database = "127.0.0.1"; 	//Servername of the database
	$user = "root";				//Username for database connection
	$password = "";				//Password for database connection
	$dbname = "bestelltool";	//Name of the MySQL database
	$send_ack = false;			//true, if a mail shall be sent to the customer after the order has been received
	$from = "skripte@fsr-imn.de";			//if an acknowledgement mail shall be sent, this is the mail address to be filled in the FROM-field of the mail
	$contact = "skripte@fsr-imn.de";		//mail address where the customer can report problems and questions to
	$beginn_summer_semester = "01.04.";		//specifies the start date of the summer semester
	$beginn_winter_semester = "01.10.";		//specifies the start date of the winter semester
	//******************************************************************************************************************************************
		
	@$connection = new mysqli($database, $user, $password, $dbname);
	@$connection2 = new mysqli($database, $user, $password, $dbname);
	
	if (substr($modules, -1, 1) != "/")
	{
		$separator = "/";
	}
	else
	{
		$separator = "";
	}
	
	if (mysqli_connect_errno())
	{
		printf("Connection error: %s\n",mysqli_connect_error());
		exit();
	}
	
	if (!isset($_GET['page']))
	{
		$_GET['page'] = 'welcome';
	}

	$datum = date('Y');
	$timestamp1 = strtotime($beginn_summer_semester.$datum);
	$timestamp2 = strtotime($beginn_winter_semester.$datum);
	$datum1 = date('n',$timestamp1);
	$datum2 = date('n',$timestamp2);	
	if (date('n') >= $datum1 AND date('n') < $datum2)
	{
		$sem = 0;
		$semester = "Sommersemester ".$datum;
	}
	else
	{
		if (date('n') < date('n',strtotime($beginn_summer_semester.$datum)))
			$datum--;
			
		$sem = 1;
		$semester = "Wintersemester ".$datum;
	}
	
	//Header of the HTML document
	echo "
	<!DOCTYPE html>
	<html>
		<head>
			<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
			<title>Fachschaftsrat IMN: Skriptbestellung</title>
			<meta name=\"keywords\" content=\"Fachschaftsrat, FSR, IMN, HTWK, Leipzig, Informatik, Mathematik, Medieninformatik\">
			<meta name=\"author\" content=\"Marvin Haagen\">
			<meta name=\"date\" content=\"".date('d.m.Y H:i:s',getlastmod())."\">
			
			<style type=\"text/css\">";
				echo "
".file_get_contents($modules.$separator."ordertool-styles.css");

				echo "
			</style>
			";
				
			$contact64 = base64_encode($contact);	//the Base64 encoded mail address will be included in the website
				
			echo "
			<script type=\"text/javascript\">";
				if ($_GET['page'] == 'order')
				{
					echo "
				window.onload = function(){display_total_price()};
				";
				}
				
				echo "
".file_get_contents($modules.$separator."ordertool-javascript.js");
							
				echo "
			</script>
		</head>
		<body>";
	//Content
	
	switch ($_GET['page'])
	{
		case 'welcome': //display main page
			include $modules.$separator."welcome.php";
			break;
		case 'order': //display the order page
			include $modules.$separator."order.php";
			break;
		case 'status': //display the status page			
			include $modules.$separator."status.php";
			break;
		case 'ack': //Acknowledgement page for script orders
			include $modules.$separator."ack.php";
			break;
		case 'info': //Info Page of the order tool
			include $modules.$separator."info.php";
			break;
	}
	//finish HTML declaration
		
	echo "
		</body>
	</html>
	";
?>
