<?php
	if ($session_valid == true AND $admin_right == true)
	{
		//Sessions aus der Datenbank lesen
		$sessions = array();
		$query = "
		SELECT sessionid, nachname, vorname, DATE_FORMAT(logindatum,'%d.%m.%Y %H:%i:%s') AS logindatum
		FROM User INNER JOIN Session
		ON User.ID = Session.userid";
		
		$result = $connection3->query($query);
		while($row = $result->fetch_assoc())
		{
			$sessions[$row['sessionid']] = array();
			$sessions[$row['sessionid']][0] = $row['vorname'];
			$sessions[$row['sessionid']][1] = $row['nachname'];
			$sessions[$row['sessionid']][2] = $row['logindatum'];
		}
		
		echo "
				<h2>Aktive Sitzungen</h2>
				<p>Hier werden die momentan aktiven Sitzungen angezeigt</p>
				<table style=\"text-align: center; border: 1px solid black; border-collapse: collapse\">
					<colgroup>
						<col>
						<col>
					</colgroup>
					<tr>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Benutzername&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Logindatum&#160;</th>
					</tr>";
				
		foreach($sessions as $value)
		{
			echo "
					<tr>
						<td>&#160;".$value[0]." ".$value[1]."&#160;</td>
						<td>&#160;".$value[2]."&#160;</td>
					</tr>";
		}
					
		echo "
				</table>";
	}
?>