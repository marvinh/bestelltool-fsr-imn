<?php
	if ($session_valid == true AND $admin_right == true)
	{
		//Caching table "User"
		$users = array();
		$query = "
		SELECT ID, Nachname, Vorname, Passwort, Admin
		FROM User";
		
		$result = $connection3->query($query);
		while($row = $result->fetch_assoc())
		{
			$users[$row['ID']] = array();
			$users[$row['ID']][0] = $row['Vorname'];
			$users[$row['ID']][1] = $row['Nachname'];
			$users[$row['ID']][2] = $row['Passwort'];
			$users[$row['ID']][3] = $row['Admin'];
		}
		
		if (isset($_POST['send0']))
		{
			//A user account shall be edited,deleted or its password shall be resetted
			$correctfields = 0;
			if (isset($_POST['id']))
			{
				$ids = implode('±',$_POST['id']);
				$ids = mysqli_real_escape_string($connection3, $ids);
				$ids = str_replace("'",'',$ids);
				$ids = strip_tags($ids);
				$ids = explode('±',$ids);
				$correctfields++;
			}
			
			if (isset($_POST['admin']))
			{
				$adm = implode('±',$_POST['admin']);
				$adm = mysqli_real_escape_string($connection3, $adm);
				$adm = str_replace("'",'',$adm);
				$adm = strip_tags($adm);
				$adm = explode('±',$adm);
			}
			else
			{
				$adm = array();
			}
			
			if (isset($_POST['reset']))
			{
				$rst = implode('±',$_POST['reset']);
				$rst = mysqli_real_escape_string($connection3, $rst);
				$rst = str_replace("'",'',$rst);
				$rst = strip_tags($rst);
				$rst = explode('±',$rst);
			}
			else
			{
				$rst = array();
			}
			
			if ($correctfields == 1)	
			{
				$pw_gen = array();
				$del_list = array();
				
				foreach($ids as $value)
				{
					if (isset($_POST['n'.$value]) AND isset($_POST['a'.$value]) AND array_key_exists($value, $users))
					{
						$name = mysqli_real_escape_string($connection3, $_POST['n'.$value]);
						$name = str_replace("'",'',$name);
						$name = strip_tags($name);
						
						if($_POST['a'.$value] == '1')
						{
							//User account shall be edited
							
							$name = trim($name);
							$pos = strrpos($name,' '); //find start of last name
							
							if ($pos === false)
							{
								$n1 = ""; //last name
								$n2 = $name; //first name
							}
							else
							{
								$n1 = substr($name, ($pos+1)); //last name
								$n2 = substr($name, 0, $pos); //first name
							}
														
							$query = "
							UPDATE User
							SET Nachname='".$n1."', Vorname='".$n2."'";
							
							//Admin-Flag setzen oder löschen
							if(in_array($value, $adm))
							{
								$query = $query.", Admin='1'";
								$users[$value][3] = '1';
							}
							else
							{
								$query = $query.", Admin='0'";
								$users[$value][3] = '0';
							}

							//Wenn gewünscht, Passwort generieren
							if(in_array($value, $rst))
							{
								$salt = gen_salt(15);
								$pw = gen_password(15);
								$pw_gen[$value] = $pw;
								$pw = hash('sha512',$pw.$salt);
								
								$query = $query.", Salt='".$salt."', Passwort='".$pw."'";
							}
							
							$query = $query."
							WHERE ID='".$value."'";
							
							$connection3->query($query);
							
							$users[$value][0] = $n2;
							$users[$value][1] = $n1;
							
							$ch_applied = true;
							$action_applied = true;
						}
						elseif($_POST['a'.$value] == '2')
						{
							//User account shall be deleted
							$del_list[] = $value;
						}
					}
				}
				
				if (count($del_list) > 0)
				{
					echo "
				<div class=\"overlay_background\">
					<div class=\"overlay_content\">
						<p style=\"font-weight: bold; margin-bottom: 1em; text-align: center\">Diese Benutzerkonten löschen?</p>
						<form action=\"".$_SERVER["PHP_SELF"]."?page=user\" method=\"post\" accept-charset=\"UTF-8\">";
										
					foreach($del_list as $value)
					{		
						echo "
								<ul><li>".$users[$value][0]." ".$users[$value][1]."</li></ul>
								<input type=\"hidden\" name=\"id[]\" value=\"".$value."\">";
					}
							
					echo "
							<table style=\"width: 100%; margin: 1em 0px\">
								<colgroup>
									<col>
									<col>
								</colgroup>
								<tr>
									<td style=\"width: 50%\"><input style=\"width: 100%\" type=\"submit\" name=\"send2\" value=\"Ja\"></td>
									<td style=\"width: 50%\"><input style=\"width: 100%\" type=\"submit\" value=\"Nein\"></td>
								</tr>
							</table>";
					
					if (count($pw_gen) > 0)
					{
						foreach($pw_gen AS $key=>$value)
						{
						echo "
							<input type=\"hidden\" name=\"pw[]\" value=\"".$users[$key][0]." ".$users[$key][1].": ".$value."\">";
						}
					}
					
					echo "
						</form>
					</div>
				</div>";
				}
				elseif (count($pw_gen) > 0)
				{
					foreach($pw_gen as $key=>$value)
					{
						$_POST['pw'][] = $users[$key][0]." ".$users[$key][1].": ".$value; //$_POST['pw'] setzen, damit die generierten Passwörter angezeigt werden
					}
				}
				
			}
		}
		if (isset($_POST['send1']))
		{
			//Benutzerkonten sollen hinzugefügt werden
			if (isset($_POST['admin']))
			{
				$adm = implode('±',$_POST['admin']);
				$adm = mysqli_real_escape_string($connection3, $adm);
				$adm = str_replace("'",'',$adm);
				$adm = strip_tags($adm);
				$adm = explode('±',$adm);
			}
			else
			{
				$adm = array();
			}
			if (isset($_POST['reset']))
			{
				$pw2 = implode('±',$_POST['reset']);
				$pw2 = mysqli_real_escape_string($connection3, $pw2);
				$pw2 = str_replace("'",'',$pw2);
				$pw2 = strip_tags($pw2);
				$pw2 = explode('±',$pw2);
			}
			else
			{
				$pw2 = array();
			}
						
			for ($counter = 0; $counter < 5; $counter++)
			{
				if(isset($_POST['n'.$counter]))
				{
					if(trim($_POST['n'.$counter]) != '')
					{
						$name = trim($_POST['n'.$counter]);
						$pos = strrpos($name,' '); //find start of last name
						
						$t = "";
						
						if ($pos === false)
						{
							$n1 = ""; //last name
							$n2 = $name; //first name
						}
						else
						{
							$n1 = substr($name, ($pos+1)); //last name
							$n2 = substr($name, 0, $pos); //first name
							$t = " ";
						}
						
						$pw = "";
						$salt = "";
						
						if(in_array($counter, $pw2))
						{
							$salt = gen_salt(15);
							$pw = gen_password(15);
							$_POST['pw'][] = $n2.$t.$n1.": ".$pw; //$_POST['pw'] setzen, damit die generierten Passwörter angezeigt werden
							$pw = hash('sha512',$pw.$salt);
						}
						
						if(in_array($counter, $adm))
						{
							$a = "1";
						}
						else
						{
							$a = "0";
						}
							
						$query = "
						INSERT INTO User (Nachname, Vorname, Passwort, Salt, Admin)
						VALUES ('".$n1."', '".$n2."', '".$pw."', '".$salt."', '".$a."')";
						
						$result = $connection3->query($query);
						
						//ID des eingefügten Datensatzes ermitteln
						$result = $connection3->query("SELECT LAST_INSERT_ID() FROM User");
						$id = implode($result->fetch_assoc());
						
						$users[$id] = array();
						$users[$id][0] = $n2;
						$users[$id][1] = $n1;
						$users[$id][2] = $pw;
						$users[$id][3] = $a;
						
						$new_applied = true;
						$action_applied = true;
					}
				}
			}
		}
		if (isset($_POST['send2']))
		{
			//Benutzerkonten sollen gelöscht werden
			if (isset($_POST['id']))
			{
				$ids = implode('±',$_POST['id']);
				$ids = mysqli_real_escape_string($connection3, $ids);
				$ids = str_replace("'",'',$ids);
				$ids = strip_tags($ids);
				$ids = explode('±',$ids);
				
				foreach($ids as $value)
				{
					if (isset($users[$value]))
					{
						$query = "
						DELETE FROM User
						WHERE ID='".$value."'";
						
						$connection3->query($query);
						
						unset($users[$value]);
						
						$del_applied = true;
						$action_applied = true;
					}
				}
			}
		}
		
		if (isset($_POST['pw']))
		{
			$new_pw = implode('±',$_POST['pw']);
			$new_pw = mysqli_real_escape_string($connection3, $new_pw);
			$new_pw = str_replace("'",'',$new_pw);
			$new_pw = strip_tags($new_pw);
			$new_pw = explode('±',$new_pw);
			
			echo "
				<div class=\"overlay_background\">
					<div class=\"overlay_content\">
						<p style=\"font-weight: bold; margin-bottom: 1em; text-align: center\">Für folgende Benutzerkonten<br>wurden neue Passwörter generiert:</p>
						<form action=\"".$_SERVER["PHP_SELF"]."?page=user\" method=\"post\" accept-charset=\"UTF-8\">";
										
			foreach($new_pw as $value)
			{		
				echo "
							<ul><li>".$value."</li></ul>";
			}
							
			echo "
							<p style=\"text-align: center; margin: 1em 0\"><input style=\"width: 33%\" type=\"submit\" value=\"OK\"></p>
						</form>
					</div>
				</div>";
		}
		
		echo "
				<h2>Benutzerkonten bearbeiten</h2>";
				
		if (!isset($action_applied) OR $action_applied == false)
		{
			echo "
				<p>Hier können Sie die Benutzer verwalten, die berechtigt sind, sich am Bestelltool anzumelden.</p>";
		}
		else
		{
			if (isset($del_applied) AND $del_applied == true)
			{
				echo "
				<p style=\"margin: 0px 2em 0px 0px; font-weight: bold; display: inline\">Benutzer wurden gelöscht</p>";
			}
			if (isset($new_applied) AND $new_applied == true)
			{
				echo "
				<p style=\"margin: 0px 2em 0px 0px; font-weight: bold; display: inline\">Benutzer wurden hinzugefügt</p>";
			}
			if (isset($ch_applied) AND $ch_applied == true)
			{
				echo "
				<p style=\"margin: 0px 2em 0px 0px; font-weight: bold; display: inline\">Benutzer wurden übernommen</p>";
			}
		}
				
		echo "
				<form action=\"".$_SERVER["PHP_SELF"]."?page=user\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"text-align: center; border: 1px solid black; border-collapse: collapse\">
					<colgroup>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<tr>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Benutzername&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Admin&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Bemerkungen&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Aktion&#160;</th>
					</tr>";
					
		foreach($users as $key=>$value)
		{
			$a = "";
			if ($value[3] == '1')
			{
				$a = " checked";
			}
			
			$b = "";
			if (trim($value[2]) == '')
			{
				$b = "kein Passwort gesetzt";
				
				if ($deny_empty == true)
				{
					$b = $b.",<br>Anmeldung nicht möglich";
				}
			}
			
			$t = "";
			if ($value[1] != '')
			{
				$t = " ";
			}
			
			echo "
					<tr style=\"border-style: none none dotted none; border-width: 1px; border-color: black\">
						<td style=\"vertical-align: top\">&#160;<input type=\"text\" name=\"n".$key."\" size=\"25\" value=\"".$value[0].$t.$value[1]."\">&#160;</td>
						<td style=\"vertical-align: top\">&#160;<input type=\"checkbox\" name=\"admin[]\" value=\"".$key."\"".$a.">&#160;</td>
						<td style=\"vertical-align: top\">&#160;".$b."&#160;</td>
						<td style=\"vertical-align: top; text-align: left\">
							&#160;<input type=\"radio\" name=\"a".$key."\" value=\"1\" checked>übernehmen&#160;
							&#160;<input type=\"radio\" name=\"a".$key."\" value=\"2\">löschen&#160;<br>
							&#160;<input type=\"checkbox\" name=\"reset[]\" value=\"".$key."\">Passwort generieren&#160;
							<input type=\"hidden\" name=\"id[]\" value=\"".$key."\">
						</td>
					</tr>";
		}
					
		echo "
					<tr style=\"text-align: right\">
						<td colspan=\"4\">&#160;<input name=\"send0\" type=\"submit\" value=\"Anwenden\">&#160;</td>
					</tr>
				</table>
				</form>
				<h2 style=\"margin-top: 2em\">Benutzerkonten hinzufügen</h2>
				<p>Hier können Sie neue Benutzerkonten hinzufügen</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=user\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"text-align: center; border: 1px solid black; border-collapse: collapse\">
					<colgroup>
						<col>
						<col>
						<col>
					</colgroup>";
					
		for ($counter = 0; $counter < 5; $counter++)
		{
			echo "
					<tr>
						<td style=\"vertical-align: top\">&#160;<input type=\"text\" name=\"n".$counter."\" size=\"25\" value=\"\">&#160;</td>
						<td style=\"vertical-align: top\">&#160;<input type=\"checkbox\" name=\"admin[]\" value=\"".$counter."\">Admin&#160;</td>
						<td style=\"vertical-align: top\">&#160;<input type=\"checkbox\" name=\"reset[]\" value=\"".$counter."\">Passwort generieren&#160;</td>
					</tr>";
		}
					
		echo "
					<tr>
						<td colspan=\"3\" style=\"text-align: right\">&#160;<input name=\"send1\" type=\"submit\" value=\"Benutzerkonten hinzufügen\">&#160;</td>
					</tr>
				</table>
				</form>";
	}
?>