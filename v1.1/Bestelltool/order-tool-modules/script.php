<?php
	if ($session_valid == true)
	{
		$fehler = false;
		$fehler2 = false;
		$fehler3 = array();
		$all_scripts = true;

		//Liste aller Dozenten aus der Datenbank laden
		$dozents = array();
						
		$query = "
		SELECT DID, Name
		FROM Dozent
		ORDER BY Name ASC";
						
		$result = $connection->query($query);
		while ($row = $result->fetch_assoc())
		{
			$dozents[$row['DID']] = $row['Name'];
		}
		
		//Liste aller Skripte aus der Datenbank laden
		$scripts2 = array();
		$query = "
		SELECT SID, Modulbezeichnung, FORMAT(Skript.Preis,2,'de_DE') AS Preis, Semester, Sommersemester, Wintersemester, Verfuegbar, 
		Dozent.DID AS DID, Name
		FROM Skript INNER JOIN Dozent
		ON Dozent.DID = Skript.DID
		ORDER BY Modulbezeichnung";
							
		$result = $connection->query($query);
		while ($row = $result->fetch_assoc())
		{
			$scripts2[$row['SID']] = array();
			$scripts2[$row['SID']][0] = $row['Modulbezeichnung'];
			$scripts2[$row['SID']][1] = $row['Preis'];
			$scripts2[$row['SID']][2] = $row['Semester'];
			$scripts2[$row['SID']][3] = $row['Sommersemester'];
			$scripts2[$row['SID']][4] = $row['Wintersemester'];
			$scripts2[$row['SID']][5] = $row['Verfuegbar'];
			$scripts2[$row['SID']][6] = $row['DID'];
			$scripts2[$row['SID']][7] = $row['Name'];
		}
		
		if (isset($_POST['send0']))
		{
			//Skripte sollen gelöscht oder aktualisiert werden
			$correctfields = 0;
			if (isset($_POST['script']))
			{
				$scripts = implode('±',$_POST['script']);
				$scripts = mysqli_real_escape_string($connection, $scripts);
				$scripts = str_replace("'",'',$scripts);
				$scripts = strip_tags($scripts);
				$scripts = explode('±',$scripts);
								
				$correctfields++;
			}
			if (isset($_POST['price']))
			{
				$price = implode('±',$_POST['price']);
				$price = mysqli_real_escape_string($connection, $price);
				$price = str_replace("'",'',$price);
				$price = strip_tags($price);
				$price = explode('±',$price);
								
				$correctfields++;
			}
			if (isset($_POST['semester']))
			{
				$semester = implode('±',$_POST['semester']);
				$semester = mysqli_real_escape_string($connection, $semester);
				$semester = str_replace("'",'',$semester);
				$semester = strip_tags($semester);
				$semester = explode('±',$semester);
							
				$correctfields++;
			}
			if (isset($_POST['did']))
			{
				$dz = implode('±',$_POST['did']);
				$dz = mysqli_real_escape_string($connection, $dz);
				$dz = str_replace("'",'',$dz);
				$dz = strip_tags($dz);
				$dz = explode('±',$dz);
								
				$correctfields++;
			}
			if (isset($_POST['sid']))
			{
				$idlist = implode(',',$_POST['sid']);
				$idlist = mysqli_real_escape_string($connection, $idlist);
				$idlist = str_replace("'",'',$idlist);
				$idlist = strip_tags($idlist);
								
				$ids = explode(',',$idlist);
								
				$correctfields++;
			}
			if ($correctfields == 5)
			{
				//Alle notwendigen Felder wurden übermittelt, Verarbeitung beginnt
																
				$amount = count($ids);
				$del_list = array();
								
				for ($index = 0; $index < $amount; $index++)
				{
					if (isset($_POST[$ids[$index]]) == false)
					{
						$fehler = true;
						break;
					}
																		
					if ($_POST[$ids[$index]] == "1")
					{
						//Die Änderungen an dem aktuellen Skript sollen übernommen werden
						if (isset($_POST['summer']))
						{
							if (in_array($ids[$index], $_POST['summer']))
							{
								$summer = '1';
							}
							else
							{
								$summer = '0';
							}
						}
						else
						{
							$summer = '0';
						}
										
						if (isset($_POST['winter']))
						{
							if (in_array($ids[$index], $_POST['winter']))
							{
								$winter = '1';
							}
							else
							{
								$winter = '0';
							}
						}
						else
						{
							$winter = '0';
						}
										
						if (isset($_POST['avail']))
						{
							if (in_array($ids[$index], $_POST['avail']))
							{
								$avail = '1';
							}
							else
							{
								$avail = '0';
							}
						}
						else
						{
							$avail = '0';
						}
																		
						if (isset($dozents[$dz[$index]]))
						{
							$query = "
							UPDATE Skript
							SET Modulbezeichnung='".$scripts[$index]."',
							Preis='".str_replace(',','.',$price[$index])."',
							Semester='".$semester[$index]."',
							Sommersemester='".$summer."',
							Wintersemester='".$winter."',
							Verfuegbar='".$avail."',
							DID='".$dz[$index]."'
							WHERE SID='".$ids[$index]."'";
								
							$result = $connection->query($query);
							
							if($connection->affected_rows > 0)
							{
								$action_applied = true;
								$ch_applied = true;
							}
														
							$scripts2[$ids[$index]] = array();
							$scripts2[$ids[$index]][0] = $scripts[$index];
							$scripts2[$ids[$index]][1] = $price[$index];
							$scripts2[$ids[$index]][2] = $semester[$index];
							$scripts2[$ids[$index]][3] = $summer;
							$scripts2[$ids[$index]][4] = $winter;
							$scripts2[$ids[$index]][5] = $avail;
							$scripts2[$ids[$index]][6] = $dz[$index];
							$scripts2[$ids[$index]][7] = $ids[$index];
						}
					}
					elseif ($_POST[$ids[$index]] == "2")
					{
						//Das aktuelle Skript soll gelöscht werden
						$del_list[] = $ids[$index];
					}
				}
				
				if (count($del_list) > 0)
				{
					echo "
				<div class=\"overlay_background\">
					<div class=\"overlay_content\">
						<p style=\"font-weight: bold; margin-bottom: 1em; text-align: center\">Diese Skripte löschen?</p>
						<form action=\"".$_SERVER["PHP_SELF"]."?page=script\" method=\"post\" accept-charset=\"UTF-8\">";
										
					foreach($del_list as $value)
					{		
						echo "
								<ul><li>".$scripts2[$value][0]."</li></ul>
								<input type=\"hidden\" name=\"sid[]\" value=\"".$value."\">";
					}
							
					echo "
							<table style=\"width: 100%; margin: 1em 0px\">
								<colgroup>
									<col>
									<col>
								</colgroup>
								<tr>
									<td style=\"width: 50%\"><input style=\"width: 100%\" type=\"submit\" name=\"send2\" value=\"Ja\"></td>
									<td style=\"width: 50%\"><input style=\"width: 100%\" type=\"submit\" value=\"Nein\"></td>
								</tr>
							</table>";
					
					if (isset($ch_applied) AND $ch_applied == true)
					{
						echo "
							<input type=\"hidden\" name=\"ch\" value\"1\">";
					}
					
					echo "
						</form>
					</div>
				</div>";
				}
			}
		}
		if (isset($_POST['send1']))
		{
			//Skripte sollen hinzugefügt werden
			$correctfields = 0;
							
			if (isset($_POST['script']))
			{
				$scripts = implode('±',$_POST['script']);
				$scripts = mysqli_real_escape_string($connection, $scripts);
				$scripts = str_replace("'",'',$scripts);
				$scripts = strip_tags($scripts);
				$scripts = explode('±',$scripts);
								
				$correctfields++;
			}
			if (isset($_POST['price']))
			{
				$price = implode('±',$_POST['price']);
				$price = mysqli_real_escape_string($connection, $price);
				$price = str_replace("'",'',$price);
				$price = strip_tags($price);
				$price = explode('±',$price);
								
				$correctfields++;
			}
			if (isset($_POST['semester']))
			{
				$semester = implode('±',$_POST['semester']);
				$semester = mysqli_real_escape_string($connection, $semester);
				$semester = str_replace("'",'',$semester);
				$semester = strip_tags($semester);
				$semester = explode('±',$semester);
								
				$correctfields++;
			}
			if (isset($_POST['did']))
			{
				$dz = implode('±',$_POST['did']);
				$dz = mysqli_real_escape_string($connection, $dz);
				$dz = str_replace("'",'',$dz);
				$dz = strip_tags($dz);
				$dz = explode('±',$dz);
								
				$correctfields++;
			}							
							
			if ($correctfields == 4)
			{
				$count1 = count($scripts);
				$count2 = count($price);
				$count3 = count($semester);
				$count3 = count($dz);
																
				if ($count1 == $count2 AND $count2 == $count3)
				{
					for ($a = 0; $a < $count1; $a++)
					{
						//Prüfen, ob in allen Zeilen, in denen mindestens eines der Eingabefelder Text enthält, die restlichen Eingabefelder ebenfalls Text enthalten.
						$line = 0;
						if (trim($scripts[$a]) != "")
						{
							$line++;
						}
						if (trim($price[$a]) != "")
						{
							$price[$a] = str_replace(',','.',$price[$a]);
							$line++;
						}
						if (trim($semester[$a]) != "")
						{
							$line++;
						}
						if (trim($dz[$a]) != "")
						{
							if (isset($dozents[$dz[$a]]))
							{
								$line++;
							}
						}
						if ($line == 4)
						{
							//Die aktuelle Zeile wurde vollständig ausgefüllt -> verarbeite Datensatz
							if (isset($_POST['summer']) AND in_array($a, $_POST['summer']))
							{
								$summer = 1;
							}
							else
							{
								$summer = 0;
							}
											
							if (isset($_POST['winter']) AND in_array($a, $_POST['winter']))
							{
								$winter = 1;
							}
							else
							{
								$winter = 0;
							}
											
							if (isset($_POST['avail']) AND in_array($a, $_POST['avail']))
							{
								$avail = 1;
							}
							else
							{
								$avail = 0;
							}
											
							$query = "
							INSERT INTO Skript (Modulbezeichnung, Preis, Semester, Sommersemester, Wintersemester, Verfuegbar, DID)
							VALUES ('".trim($scripts[$a])."','".trim($price[$a])."','".trim($semester[$a])."','".$summer."','".$winter."','".$avail."','".$dz[$a]."')";
											
							$result = $connection->query($query);
							
							//ID des eingefügten Datensatzes ermitteln
							$result = $connection->query("SELECT LAST_INSERT_ID() FROM Skript");
							$sid = implode($result->fetch_assoc());
							
							$action_applied = true;
							$new_applied = true;
							
							$scripts2[$sid] = array();
							$scripts2[$sid][0] = $scripts[$a];
							$scripts2[$sid][1] = $price[$a];
							$scripts2[$sid][2] = $semester[$a];
							$scripts2[$sid][3] = $summer;
							$scripts2[$sid][4] = $winter;
							$scripts2[$sid][5] = $avail;
							$scripts2[$sid][6] = $dz[$a];
							$scripts2[$sid][7] = $dozents[$dz[$a]];
						}
					}
				}
			}
		}
		if (isset($_POST['send2']))
		{
			//Skripte sollen gelöscht werden
			if (isset($_POST['sid']))
			{
				$scripts = implode('±',$_POST['sid']);
				$scripts = mysqli_real_escape_string($connection, $scripts);
				$scripts = str_replace("'",'',$scripts);
				$scripts = strip_tags($scripts);
				$scripts = explode('±',$scripts);
				
				foreach($scripts as $value)
				{
					//Prüfen, ob Skript noch mit Bestellungen verknüpft ist
					$query = "
					SELECT SID
					FROM Skriptbestellung
					WHERE SID='".$value."'
					LIMIT 1";
										
					$result = $connection->query($query);
					if ($row = $result->fetch_assoc())
					{
						//Es ist noch mindestens eine Bestellung mit dem Skript verknüpft -> Fehlermeldung ausgeben
						$fehler2 = true; //zeigt an, dass mindestens 1 Skript nicht gelöscht werden konnte
						$fehler3[] = $value; //aktuelle SkriptID zum Array hinzugefügen -> ermöglicht Darstellung der Skripte, die nicht gelöscht werden konnten
					}
					else
					{
						//Es sind keine Bestellungen mehr mit dem Skript verknüpft -> Skript löschen
										
						$query = "
						DELETE FROM StudiengangSkript
						WHERE SID='".$value."'";
											
						$result = $connection->query($query);
																						
						$query = "
						DELETE FROM Skript
						WHERE SID='".$value."'";
										
						$result = $connection->query($query);
						
						$action_applied = true;
						$del_applied = true;
						
						unset($scripts2[$value]);
					}
				}
			}
		}
		
		echo "
				<h2>Skripte bearbeiten</h2>";
		
		if (!isset($action_applied) OR $action_applied == false)
		{
			echo "
				<p style=\"margin: 0px 0px\">Hier können Sie die im System hinterlegten Skripte bearbeiten</p>";
		}
		else
		{
			if (isset($del_applied) AND $del_applied == true)
			{
				echo "
				<p style=\"margin: 0px 2em 0px 0px; font-weight: bold; display: inline\">Skripte wurden gelöscht</p>";
			}
			if (isset($new_applied) AND $new_applied == true)
			{
				echo "
				<p style=\"margin: 0px 2em 0px 0px; font-weight: bold; display: inline\">Skripte wurden hinzugefügt</p>";
			}
			if (isset($ch_applied) AND $ch_applied == true)
			{
				echo "
				<p style=\"margin: 0px 2em 0px 0px; font-weight: bold; display: inline\">Änderungen wurden übernommen</p>";
			}
		}
				
		echo "
				<form action=\"".$_SERVER["PHP_SELF"]."?page=script\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black; border-collapse: collapse\">
					<colgroup>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<tr>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Modulbezeichnung&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Preis&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Semester&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Verfügbarkeit&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Bestellbarkeit&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Dozent&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Aktion&#160;</th>
					</tr>";
					
		$index = 1;
						
		foreach($scripts2 as $key=>$value)
		{
			echo "
					<tr>
						<td style=\"text-align: center\">&#160;<input name=\"script[]\" type=\"text\" size=\"25\" value=\"".$value[0]."\" tabindex=\"".$index."\" required>&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"price[]\" type=\"text\" size=\"3\" value=\"".$value[1]."\" style=\"text-align: right\" tabindex=\"".($index+1)."\" required>&#160;€&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"semester[]\" type=\"text\" size=\"6\" value=\"".$value[2]."\" style=\"text-align: right\" tabindex=\"".($index+2)."\" required>&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"summer[]\" type=\"checkbox\" value=\"".$key."\" ";
			if ($value[3] == 1)
			{
				echo "checked";
			}
			echo " tabindex=\"".($index+3)."\">Sommersemester&#160;".
			"<input name=\"winter[]\" type=\"checkbox\" value=\"".$key."\" ";
			if ($value[4] == 1)
			{
				echo "checked";
			}
			echo " tabindex=\"".($index+4)."\">Wintersemester&#160;</td>
						<td style=\"text-align: center\"><input name=\"avail[]\" type=\"checkbox\" value=\"".$key."\"";
			if ($value[5] == 1)
			{
				echo " checked";
			}
			echo " tabindex=\"".($index+5)."\">&#160;bestellbar&#160;</td>
						<td>
							<select name=\"did[]\" size=\"1\" tabindex=\"".($index+6)."\">";
			foreach($dozents as $id => $name)
			{
				echo "
								<option value=\"".$id."\"";
				if ($id == $value[6])
				{
					echo " selected";
				}
				echo ">".$name."</option>";
			}									
			echo "
							</select>
						</td>
						<td>
							&#160;&#160;<input name=\"".$key."\" type=\"radio\" value=\"1\" tabindex=\"".($index+7)."\" checked>übernehmen<input name=\"".$key."\" type=\"radio\" value=\"2\" tabindex=\"".($index+8)."\">löschen";

			if (in_array($key,$fehler3))
			{
				//Skript konnte nicht gelöscht werden->zeige es dem Nutzer durch ein angehängtes Sternchen an
				echo "<span class=\"error\">*</span>";
			}
								
			echo "
							<input type=\"hidden\" name=\"sid[]\" value=\"".$key."\">&#160;
						</td>
					</tr>";
			$index = $index+9;
		}
													
		echo "
					<tr style=\"text-align: right\">
						<td colspan=\"7\">";
							
		if ($fehler2 == true)
		{
			echo "<span class=\"error\">Fehler: Einige Skripte konnten nicht gelöscht werden, da sie noch mit Bestellungen verknüpft sind&#160;</span>";
		}
						
		echo "<input name=\"send0\" type=\"submit\" value=\"Anwenden\"></td>
					</tr>
				</table>
				</form>";
		if ($fehler == true)
		{
			echo "<p class=\"error\">Bei der Verarbeitung der Daten ist ein Fehler aufgetreten</p>";
		}
						
		echo "
				<h2 style=\"margin-top: 2em\">Skripte hinzufügen</h2>
				<p>Hier können Sie neue Skripte hinzufügen</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=script\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black; border-collapse: collapse\">
					<colgroup>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<tr>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Modulbezeichnung&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Preis&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Semester&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Verfügbarkeit&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Bestellbarkeit&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Dozent&#160;</th>
					</tr>";
		for ($a = 0; $a < 5; $a++)
		{
			echo "
					<tr>
						<td style=\"text-align: center\">&#160;<input name=\"script[]\" type=\"text\" size=\"25\" tabindex=\"".$index."\">&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"price[]\" type=\"text\" size=\"3\" style=\"text-align: right\" tabindex=\"".($index+1)."\">&#160;€&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"semester[]\" value=\"\" type=\"text\" size=\"6\" style=\"text-align: right\" tabindex=\"".($index+2)."\">&#160;</td>
						<td style=\"text-align: center\">&#160;<input name=\"summer[]\" type=\"checkbox\" value=\"".$a."\" tabindex=\"".($index+3)."\">Sommersemester&#160;".
						"<input name=\"winter[]\" type=\"checkbox\" value=\"".$a."\" tabindex=\"".($index+4)."\">Wintersemester&#160;</td>
						<td style=\"text-align: center\"><input name=\"avail[]\" type=\"checkbox\" value=\"".$a."\" tabindex=\"".($index+5)."\">&#160;bestellbar&#160;</td>
						<td style=\"text-align: center\">
							<select name=\"did[]\" size=\"1\" tabindex=\"".($index+6)."\">";
									
			reset($dozents);
			echo "
								<option value=\"".key($dozents)."\" selected >".current($dozents)."</option>";
									
			$count = count($dozents);
									
			for($b = 1; $b < $count; $b++)
			{
				next($dozents);
				echo "
								<option value=\"".key($dozents)."\">".current($dozents)."</option>";
			}	

			$index = $index+6;
			echo "
							</select>
						</td>
					</tr>";
		}
							
		echo "
					<tr style=\"text-align: right\">
						<td colspan=\"6\"><input name=\"send1\" type=\"submit\" value=\"Skripte hinzufügen\"></td>
					</tr>
				</table>
				</form>";
	}
?>