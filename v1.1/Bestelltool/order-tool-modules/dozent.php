<?php
	if ($session_valid == true)
	{
		$fehler = false;
		$fehler2 = array();
		$correctfields = 0;
		
		//Dozenten cachen
		$query = "
		SELECT DID, Name, pAktualisierung
		FROM Dozent
		ORDER BY Name ASC";
					
		$result = $connection->query($query);
		
		$dozents = array();
		
		while ($row = $result->fetch_assoc())
		{
			$dozents[$row['DID']] = array();
			$dozents[$row['DID']][0] = $row['Name'];
			$dozents[$row['DID']][1] = $row['pAktualisierung'];			
		}
		
		if (isset($_POST['send0']))
		{
			//Dozenten sollen bearbeitet oder gelöscht werden
			/*(für alle zu löschenden Dozenten wird hier nur eine Löschnachfrage angezeigt,
			 *das eigentliche Löschen erfolgt über einen anderen $_POST-Parameter)
			 */
							
			if (isset($_POST['name']))
			{
				$names = implode('±',$_POST['name']);
				$names = mysqli_real_escape_string($connection, $names);
				$names = str_replace("'",'',$names);
				$names = strip_tags($names);
				$names = explode('±',$names);
								
				$correctfields++;
			}
			if (isset($_POST['did']))
			{
				$dz = implode('±',$_POST['did']);
				$dz = mysqli_real_escape_string($connection, $dz);
				$dz = str_replace("'",'',$dz);
				$dz = strip_tags($dz);
				$dz = explode('±',$dz);
								
				$correctfields++;
			}
			if (isset($_POST['update']))
			{
				$up = implode('±',$_POST['update']);
				$up = mysqli_real_escape_string($connection, $up);
				$up = str_replace("'",'',$up);
				$up = strip_tags($up);
				$up = explode('±',$up);
			}
			else
			{
				$up = array();
			}	
			
			if ($correctfields == 2)
			{
				$count = count($dz);
				$del_list = array();
							
				for ($a = 0; $a < $count; $a++)
				{
					if (in_array($dz[$a], $up))
					{
						$pup = 1;
					}
					else
					{
						$pup = 0;
					}
									
					if (isset($_POST[$dz[$a]]))
					{
						if ($_POST[$dz[$a]] == '1')
						{
							//Dozent soll aktualisiert werden
							$query = "
							UPDATE Dozent
							SET Name='".$names[$a]."', pAktualisierung='".$pup."'
							WHERE DID='".$dz[$a]."'";
											
							$result = $connection->query($query);
							
							if ($connection->affected_rows > 0)
							{
								$ch_applied = true;
								$action_applied = true;
							}
							
							$dozents[$dz[$a]][0] = $names[$a];
							$dozents[$dz[$a]][1] = $pup;
						}
						elseif ($_POST[$dz[$a]] == '2')
						{
							//Dozent soll gelöscht werden
							$del_list[] = $dz[$a];
						}
					}
				}
								
				if (count($del_list) > 0)
				{
					echo "
				<div class=\"overlay_background\">
					<div class=\"overlay_content\">
						<p style=\"font-weight: bold; margin-bottom: 1em; text-align: center\">Diese Dozenten löschen?</p>
						<form action=\"".$_SERVER["PHP_SELF"]."?page=dozent\" method=\"post\" accept-charset=\"UTF-8\">";
					
					foreach($del_list as $value)
					{						
						echo "
								<ul><li>".$dozents[$value][0]."</li></ul>
								<input type=\"hidden\" name=\"did[]\" value=\"".$value."\">";
					}
							
					echo "
							<table style=\"width: 100%; margin: 1em 0px\">
								<colgroup>
									<col>
									<col>
								</colgroup>
								<tr>
									<td style=\"width: 50%\"><input style=\"width: 100%\" type=\"submit\" name=\"send2\" value=\"Ja\"></td>
									<td style=\"width: 50%\"><input style=\"width: 100%\" type=\"submit\" value=\"Nein\"></td>
								</tr>
							</table>";
					
					if (isset($ch_applied) AND $ch_applied == true)
					{
						echo "
							<input type=\"hidden\" name=\"ch\" value\"1\">";
					}
					
					echo "
						</form>
					</div>
				</div>";
				}
			}
		}
		if (isset($_POST['send1']))
		{
			//Dozenten sollen hinzugefügt werden
			if (isset($_POST['name']))
			{
				$names = implode('±',$_POST['name']);
				$names = mysqli_real_escape_string($connection, $names);
				$names = str_replace("'",'',$names);
				$names = strip_tags($names);
				$names = explode('±',$names);
								
				$correctfields++;
			}
			if (isset($_POST['update']))
			{
				$up = implode('±',$_POST['update']);
				$up = mysqli_real_escape_string($connection, $up);
				$up = str_replace("'",'',$up);
				$up = strip_tags($up);
				$up = explode('±',$up);
			}
			else
			{
				$up = array();
			}
			if ($correctfields == 1)
			{
				$count = count($names);
				foreach ($names as $key=>$value)
				{
					if (trim($value) != "")
					{
						if (in_array($key, $up))
						{
							$pup = 1;
						}
						else
						{
							$pup = 0;
						}
									
						$query = "
						INSERT INTO Dozent (Name, pAktualisierung)
						VALUES ('".$value."','".$pup."')";
									
						$result = $connection->query($query);
						
						//ID des eingefügten Datensatzes ermitteln
						$result = $connection->query("SELECT LAST_INSERT_ID() FROM Dozent");
						$did = implode($result->fetch_assoc());
						
						$dozents[$did] = array();
						$dozents[$did][0] = $value;
						$dozents[$did][1] = $pup;
												
						$new_applied = true;
						$action_applied = true;
					}
				}
			}
		}
		if (isset($_POST['send2']))
		{
			//Dozenten sollen gelöscht werden	
			if (isset($_POST['ch']))
			{
				$ch_applied = true;
				$action_applied = true;
			}
			if (isset($_POST['did']))
			{
				$dz = implode('±',$_POST['did']);
				$dz = mysqli_real_escape_string($connection, $dz);
				$dz = str_replace("'",'',$dz);
				$dz = strip_tags($dz);
				$dz = explode('±',$dz);
				
				foreach($dz as $value)
				{
					//Prüfen, ob dem Dozent noch ein Skript zugeordnet ist
					$query = "
					SELECT DID FROM Skript
					WHERE DID='".$value."'
					LIMIT 1";
								
					$result = $connection->query($query);
					if ($row = $result->fetch_assoc())
					{
						//Dem Dozent ist noch ein Skript zugeordnet -> zeige Fehlermeldung an
						$fehler = true;
						$fehler2[] = $value;
					}
					else
					{
						//Dem Dozent ist kein Skript zugeordnet -> lösche den Dozent
						$query = "
						DELETE FROM Dozent
						WHERE DID='".$value."'";
			
						$result = $connection->query($query);
						
						unset($dozents[$value]);
					}
				}
				
				$del_applied = true;
				$action_applied = true;
			}
		}
		echo "
				<h2>Dozenten bearbeiten</h2>";

		if (isset($new_applied) AND $new_applied == true)
		{
			echo "
				<p style=\"margin: 0px 2em 0px 0px; font-weight: bold; display: inline\">Dozenten wurden hinzugefügt</p>";
		}
		else
		{
			if (isset($del_applied) AND $del_applied == true)
			{
				echo "
				<p style=\"margin: 0px 2em 0px 0px; font-weight: bold; display: inline\">Dozenten wurden gelöscht</p>";
			}
			if (isset($ch_applied) AND $ch_applied == true)
			{
				echo "
				<p style=\"margin: 0px 2em 0px 0px; font-weight: bold; display: inline\">Änderungen wurden übernommen</p>";
			}
			if (!isset($action_applied) OR $action_applied == false)
			{
				echo "
				<p style=\"margin: 0px 0px\">Hier können Sie die im System hinterlegten Dozenten bearbeiten</p>";
			}
		}
		
		echo "
				<form action=\"".$_SERVER["PHP_SELF"]."?page=dozent\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black; border-collapse: collapse\">
					<colgroup>
						<col>
						<col>
						<col>
					</colgroup>
					<tr>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Name des Dozenten&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Partielle Aktualisierung&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Aktion&#160;</th>
					</tr>";
				
		$index = 1;
		foreach($dozents as $key=>$value)
		{
			echo "
					<tr>
						<td>&#160;<input name=\"name[]\" type=\"text\" size=\"25\" value=\"".$value[0]."\" tabindex=\"".$index."\" required>&#160;</td>
						<td>
							<input name=\"update[]\" type=\"checkbox\" value=\"".$key."\" ";
								
			if ($value[1] == 1)
			{
				echo "checked ";
			}
								
			echo "tabindex=\"".($index+1)."\">Skripte werden partiell aktualisiert&#160;
							<input type=\"hidden\" name=\"did[]\" value=\"".$key."\">&#160;
						</td>
						<td>&#160;<input name=\"".$key."\" type=\"radio\" value=\"1\" tabindex=\"".($index+2)."\" checked>übernehmen<input name=\"".$key."\" type=\"radio\" value=\"2\" tabindex=\"".($index+3)."\">löschen&#160;";
								
			if (in_array($key, $fehler2))
			{
				echo "<span class=\"error\">*</span>";
			}
								
			echo "</td>
					</tr>";
			$index = $index+4;
		}
											
		echo "
					<tr style=\"text-align: right\">
						<td colspan=\"3\"><input name=\"send0\" type=\"submit\" value=\"Anwenden\"></td>
					</tr>
				</table>
				</form>";
						
		if ($fehler == true)
		{
			echo "
				<p class=\"error\" style=\"margin: 0px\">Fehler: Einige Dozenten konnten nicht gelöscht werden, da ihnen noch Skripte zugeordnet sind</p>";
		}
						
		echo "
				<h2 style=\"margin-top: 2em\">Dozenten hinzufügen</h2>
				<p>Hier können Sie neue Dozenten hinzufügen</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=dozent\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black; border-collapse: collapse\">
					<colgroup>
						<col>
						<col>
					</colgroup>
					<tr>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Name des Dozenten&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Partielle Aktualisierung&#160;</th>
					</tr>";
						
		for ($a = 0; $a < 5; $a++)
		{
			echo "
					<tr>
						<td>&#160;<input name=\"name[]\" type=\"text\" size=\"25\" value=\"\" tabindex=\"".$index."\">&#160;</td>
						<td><input name=\"update[]\" type=\"checkbox\" value=\"".$a."\" tabindex=\"".($index+1)."\">Skripte werden partiell aktualisiert&#160;</td>
					</tr>";
			$index = $index+2;
		}
							
		echo "
					<tr style=\"text-align: right\"><td colspan=\"2\"><input name=\"send1\" type=\"submit\" value=\"Dozenten hinzufügen\"></td></tr>
				</table>
				</form>";
	}
?>