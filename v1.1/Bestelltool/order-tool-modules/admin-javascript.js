				function display_total_price()
				{
					var price = 0.0;
					var total = 0.0;
					var amount = 0;
					var output = '';
					var counter = 0;
					var finish = false;
					
					while(finish == false)
					{
						if (document.getElementById('tp'+counter))
						{
							output = 'Gesamtpreis der Auswahl: '+total.toFixed(2)+' €';
							output = output.replace('.',',');
							document.getElementById('tp'+counter).innerHTML = output;
							price = 0.0;
							total = 0.0;
							amount = 0;
						}
						
						if (document.getElementById('p'+counter) && document.getElementById('c'+counter) && document.getElementById('a'+counter))
						{
							if (document.getElementById('c'+counter).checked)
							{
								price = document.getElementById('p'+counter).innerHTML;
								price = price.replace(',','.');
								price = parseFloat(price.replace(' €',''));
									
								amount = parseFloat(document.getElementById('a'+counter).value);
									
								price = price*amount;
								total = total+price;
							}
						}
						else
						{
							finish = true;
						}
						counter++;
					}
				}