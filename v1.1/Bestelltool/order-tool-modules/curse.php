<?php
	if ($session_valid == true)
	{
		$correctfields = 0;
		
		//Studiengänge cachen
		$query = "
		SELECT StID, Bezeichnung
		FROM Studiengang
		ORDER BY Bezeichnung ASC";
		
		$result = $connection->query($query);
		
		$curses = array();
		
		while($row = $result->fetch_assoc())
		{
			$curses[$row['StID']] = $row['Bezeichnung'];
		}
		
		//=>Ereignisse verarbeiten
		if (isset($_GET['cid']))
		{
			$_GET['cid'] = mysqli_real_escape_string($connection, $_GET['cid']);
			$_GET['cid'] = str_replace("'",'',$_GET['cid']);
			$_GET['cid'] = strip_tags($_GET['cid']);
		}
		
		if (isset($_POST['send0']))
		{
			//Studiengänge sollen bearbeitet werden
			if (isset($_POST['cid']))
			{
				$cid = mysqli_real_escape_string($connection, $_POST['cid']);
				$cid = str_replace("'",'',$cid);
				$cid = strip_tags($cid);
				
				$correctfields++;
			}
			if (isset($_POST[$cid]))
			{
				$cname = mysqli_real_escape_string($connection, $_POST[$cid]);
				$cname = str_replace("'",'',$cname);
				$cname = strip_tags($cname);
				
				$correctfields++;
			}
			if ($correctfields == 2)
			{	
				$ch_applied = false;
				//zuerst den Namen des Skripts updaten
				$query = "
				UPDATE Studiengang
				SET Bezeichnung='".$cname."'
				WHERE StID='".$cid."'";
				
				$result = $connection->query($query);
				
				if ($connection->affected_rows > 0)
				{
					$ch_applied = true;
				}
			
				//danach alle Datensätze aus StudiengangSkript löschen, die mit dem aktuellen Studiengang verknüpft sind
				$query = "
				DELETE FROM StudiengangSkript
				WHERE StID='".$cid."'";
				
				$result = $connection->query($query);
								
				//nun die neuen Datensätze in StudiengangSkript einfügen	
				if (isset($_POST['sid']) AND array_key_exists($cid, $curses))
				{
					$sid = implode('±',$_POST['sid']);
					$sid = mysqli_real_escape_string($connection, $sid);
					$sid = str_replace("'",'',$sid);
					$sid = strip_tags($sid);
					$sid = explode('±',$sid);
					
					foreach($sid as $value)
					{
						$query = "
						INSERT INTO StudiengangSkript (SID, StID)
						VALUES ('".$value."','".$cid."')";
					
						$result = $connection->query($query);
					}
				}
				
				if ($connection->affected_rows > 0)
				{
					$ch_applied = true;
				}
			}
		}
		if (isset($_POST['send1']))
		{
			//Ein Studiengang soll hinzugefügt werden
			if (isset($_POST['cid']))
			{
				$cid = mysqli_real_escape_string($connection, $_POST['cid']);
				$cid = str_replace("'",'',$cid);
				$cid = strip_tags($cid);
				
				$correctfields++;
			}
			if (isset($_POST[$cid]))
			{
				$cname = mysqli_real_escape_string($connection, $_POST[$cid]);
				$cname = str_replace("'",'',$cname);
				$cname = strip_tags($cname);
				
				$correctfields++;
			}
			if ($correctfields == 2)
			{
				//Studiengang einfügen
				$query = "
				INSERT INTO Studiengang (Bezeichnung)
				VALUES ('".$cname."')";
				
				$result = $connection->query($query);
				
				//ID des eingefügten Datensatzes ermitteln
				$result = $connection->query("SELECT LAST_INSERT_ID() FROM Studiengang");
				$cid = implode($result->fetch_assoc());
				
				$curses[$cid] = $cname;
				
				if (isset($_POST['sid']))
				{
					$sid = implode('±',$_POST['sid']);
					$sid = mysqli_real_escape_string($connection, $sid);
					$sid = str_replace("'",'',$sid);
					$sid = strip_tags($sid);
					$sid = explode('±',$sid);
					
					foreach($sid as $value)
					{
						$query = "
						INSERT INTO StudiengangSkript (SID, StID)
						VALUES ('".$value."','".$cid."')";
					
						$result = $connection->query($query);
					}
				}
				$new_applied = true;
			}
		}
		if (isset($_POST['send3']))
		{
			//Ein Skript soll gelöscht werden
			if (isset($_POST['cid']))
			{
				$cid = mysqli_real_escape_string($connection, $_POST['cid']);
				$cid = str_replace("'",'',$cid);
				$cid = strip_tags($cid);
				
				$query = "
				DELETE FROM StudiengangSkript
				WHERE StID='".$cid."'";
				
				$result = $connection->query($query);
				
				$query = "
				DELETE FROM Studiengang
				WHERE StID='".$cid."'";
				
				$result = $connection->query($query);
				
				$del_applied = true;
				unset($curses[$cid]);
				unset($_GET['cid']);
			}
		}		
		if (isset($_POST['send4']))
		{
			//Nachfrage, ob der Studiengang wirklich gelöscht werden soll
			if (isset($_POST['cid']) AND isset($curses[$_POST['cid']]))
			{
				echo "
				<div class=\"overlay_background\">
					<div class=\"overlay_content\">
						<p style=\"font-weight: bold; margin: 1em 0px 0px 0px\">Studiengang \"".$curses[$_POST['cid']]."\" löschen?</p>
						<form action=\"".$_SERVER["PHP_SELF"]."?page=curse&cid=".$_POST['cid']."\" method=\"post\" accept-charset=\"UTF-8\">
							<input type=\"hidden\" name=\"cid\" value=\"".$_POST['cid']."\">
							<table style=\"width: 100%; margin-bottom: 1em\">
								<colgroup>
									<col>
									<col>
								</colgroup>
								<tr>
									<td style=\"width: 50%\"><input style=\"width: 100%\" type=\"submit\" name=\"send3\" value=\"Ja\"></td>
									<td style=\"width: 50%\"><input style=\"width: 100%\" type=\"submit\" value=\"Nein\"></td>
								</tr>
							</table>
						</form>
					</div>
				</div>";
			}
		}
		//=>Verarbeitung der Ereignisse abgeschlossen
		
		//Auswahlfeld für Studiengänge anzeigen
		echo "
				<h2>Studiengänge bearbeiten</h2>
				<p>Bitte wählen Sie den Studiengang aus, den Sie bearbeiten möchten:</p>
				<form action=\"".$_SERVER["PHP_SELF"]."?page=curse\" method=\"post\" accept-charset=\"UTF-8\">
					<select name=\"cid\">";
		
		foreach($curses as $key=>$value)
		{
			if (isset($_GET['cid']) AND $_GET['cid'] == $key)
			{
				echo "
						<option value=\"".$key."\" selected>".$value."</option>";
			}
			else
			{
				echo "
						<option value=\"".$key."\">".$value."</option>";
			}
		}
		
		//Anmerkung: Das Verhalten, wenn $_POST['send2'] gesetzt ist, wird in admin.php ganz am Anfang abgearbeitet,
		//da ein Location-Header gesendet werden muss, der ein Neuladen der Seite nach sich zieht.
		//Dies muss vor der ersten Ausgabe geschehen, um Fehler zu vermeiden, deshalb befindet es sich in admin.php
		echo "
					</select>
					<input name=\"send2\" type=\"submit\" value=\"Studiengang anzeigen\">
					<input name=\"send4\" type=\"submit\" value=\"Studiengang löschen\">
				</form>";
				
		if (isset($ch_applied) AND $ch_applied == true)
		{
			$buffer = "Änderungen wurden übernommen";
		}
		else
		{
			if (isset($new_applied) AND $new_applied == true)
			{
				$buffer = "Studiengang wurde hinzugefügt";
			}
			else
			{
				if (isset($del_applied) AND $del_applied == true)
				{
					$buffer = "Studiengang wurde gelöscht";
				}
				else
				{
					$buffer = "&#160;";
				}
			}
		}
		
		echo "
				<p style=\"margin-top: 0.5em; font-weight: bold\">".$buffer."</p>";
				
		$size = count($curses);
		
		//Skripte cachen
		$query = "
		SELECT SID, Modulbezeichnung
		FROM Skript
		ORDER BY Modulbezeichnung ASC";
						
		$result = $connection->query($query);
						
		$scripts = array();
						
		while ($row = $result->fetch_assoc())
		{
			$scripts[$row['SID']] = $row['Modulbezeichnung'];
		}
		
		if (isset($_GET['cid']) AND isset($curses[$_GET['cid']]))
		{
		
			//Tabelle "StudiengangSkript" cachen
			$query = "
			SELECT StID, SID
			FROM StudiengangSkript";
						
			$result = $connection->query($query);
							
			$selection = array();
						
			while ($row = $result->fetch_assoc())
			{
				if (isset($selection[$row['StID']]))
				{
					$selection[$row['StID']] = $selection[$row['StID']]."-".$row['SID']."-";
				}
				else
				{
					$selection[$row['StID']] = "-".$row['SID']."-";
				}
			}
			
			echo "
				<form style=\"margin-top: 1.5em; margin-bottom: 0.5em;\" action=\"".$_SERVER["PHP_SELF"]."?page=curse&cid=".$_GET['cid']."\" method=\"post\" accept-charset=\"UTF-8\">
				<input name=\"".$_GET['cid']."\" size=\"50\" type=\"text\" style=\"font-weight: bold\" value=\"".$curses[$_GET['cid']]."\">
				<input type=\"hidden\" name=\"cid\" value=\"".$_GET['cid']."\">";
		}
		else
		{
			echo "
				<form style=\"margin-top: 1.5em; margin-bottom: 0.5em;\" action=\"".$_SERVER["PHP_SELF"]."?page=curse&cid=new\" method=\"post\" accept-charset=\"UTF-8\">
				<input name=\"new\" size=\"50\" type=\"text\" style=\"font-weight: bold\" value=\"\">
				<input type=\"hidden\" name=\"cid\" value=\"new\">";
		}
		
		//Skripte anzeigen				
		echo "
				<table>
					<colgroup>
						<col style=\"width: 50%\">
						<col style=\"width: 50%\">
					</colgroup>";
			
		$odd = false;
		foreach($scripts as $key=>$value)
		{
			$selected = "";
			if (isset($_GET['cid']) AND isset($selection[$_GET['cid']]))
			{
				if (strpos($selection[$_GET['cid']],"-".$key."-") === false)
				{
				}
				else
				{
					$selected = " checked";
				}
			}
				
			if ($odd == false)
			{
				$odd = true;
				
				echo "
					<tr>
						<td><input name=\"sid[]\" value=\"".$key."\" type=\"checkbox\"".$selected.">".$value."</td>";
			}
			else
			{
				$odd = false;
				echo "
						<td><input name=\"sid[]\" value=\"".$key."\" type=\"checkbox\"".$selected.">".$value."</td>
					</tr>";
			}
		}
		if ($odd == true)
		{
			echo "
						<td></td>
					</tr>";
		}
			
		echo "
					<tr>
						<td colspan=\"2\" style=\"text-align: right\">
							<input name=\"send0\" type=\"submit\" value=\"Änderungen übernehmen\">
							<input name=\"send1\" type=\"submit\" value=\"Studiengang hinzufügen\">
						</td>
					</tr>
				</table>
				</form>";
	}
?>