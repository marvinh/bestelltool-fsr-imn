				function display_total_price()
				{
					var price = 0.0;
					var total = 0.0;
					var output = '';
					var counter = 1;
					var finish = false;
					
					while(finish == false)
					{
						if (document.getElementById('p'+counter))
						{
							if (document.getElementsByName('script[]')[counter-1].checked)
							{
								price = document.getElementById('p'+counter).innerHTML;
								price = price.replace(',','.');
								price = parseFloat(price.replace(' €',''));
								total = total+price;
							}
						}
						else
						{
							finish = true;
						}
						counter++;
					}
					
					output = 'Gesamtpreis: '+total.toFixed(2)+' €';
					output = output.replace('.',',');
					document.getElementById('totalprice').innerHTML = output;
				}

				function decode_uri()
				{
					var result = '';
					var input = '$contact64';
					var c = new Array(4);
					var o = new Array(0);
					var r1 = '';
					var r2 = '';
					var r3 = '';
					
					for (var i = 0; i < input.length; i++)
					{
						c[0] = input.charAt(i++);
						c[1] = input.charAt(i++);
						c[2] = input.charAt(i++);
						c[3] = input.charAt(i);
					
						for (var a = 0; a < 4; a++)
						{
							o[a] = c[a].charCodeAt(0);
							
							if (o[a] > 47 && o[a] < 58)
								o[a] = o[a]+4;
							else
								if(o[a] > 64 && o[a] < 91)
									o[a] = o[a]-65;
								else
									if (o[a] > 96 && o[a] < 123)
										o[a] = o[a]-71;
									else
										if (o[a] == 43)
											o[a] = 62;
										else
											if (o[a] == 61)
												o[a] = 0;
											else
												o[a] = 63;
						}
					
						r1 = String.fromCharCode((o[0] << 2) | (o[1] >> 4));
						r2 = String.fromCharCode(((o[1] & 0xF) << 4) | (o[2] >> 2));
						r3 = String.fromCharCode(((o[2] & 0x3) << 6) | o[3]);
						
						result=result+r1+r2+r3;
					}
					
					result = 'mailto:'+result;
					window.location = result;
				}