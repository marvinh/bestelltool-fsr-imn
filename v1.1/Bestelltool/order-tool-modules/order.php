<?php
	//Status der Bestell-Sperre abfragen
	$query = "
	SELECT ID
	FROM Status
	WHERE ID='1'
	LIMIT 1";
	$result = $connection->query($query);
	if ($row = $result->fetch_assoc())
	{
		//Bestellungsannahme ist gesperrt
		$dblock = true;
	}
	else
	{
		//Bestellungsannahme ist freigegeben
		$dblock = false;
	}
		
	if ($dblock == false)
	{
		echo "
			<div class=\"order_page\">
				<h1>Skriptbestellung des FSR IMN</h1>
				<br>
				<h2>Studiengang</h2>";
		
		//Studiengänge anzeigen und gleichzeitig cachen für weitere Zugriffe
		$curses = array();
		$query = "
		SELECT DISTINCT StID, Bezeichnung FROM studiengang";
		
		$result = $connection->query($query);
		
		while ($row = $result->fetch_assoc())
		{
			echo "
				<a href=\"".$_SERVER['SCRIPT_NAME']."?page=order&cid=".$row['StID']."\">".$row['Bezeichnung']."</a><br>";
			$curses[$row['StID']] = $row['Bezeichnung'];
		}
				
		if(isset($_GET['cid']))
		{
			//if a course has been selected, display available scripts
				
			$_GET['cid'] = mysqli_real_escape_string($connection,$_GET['cid']);
			$_GET['cid'] = str_replace("'","",$_GET['cid']);
			$_GET['cid'] = strip_tags($_GET['cid']);
								
			if(isset($curses[$_GET['cid']]))
			{
				//if there are scripts available, display form if
				//it hasn't been sent already or process the transmitted form
					
				echo "<br>
				<div class=\"order_form\">
					<table>
						<tr>
							<td  class=\"order_form\">";
					
				$fina_default = "";	//default value of the first name field
				$fana_default = "";	//default value of the family name field
				$mail_default = ""; //default value of the mail address field
				$correctfields = 0;
				$idlist = "''";
					
				if ($send_ack == true)
				{
					//if an acknowledgement mail shall be sent, skip the display of the total
					//price because it is contained in the acknowledgement mail
					$buttonname = "ok";
				}
				else
				{
					//no acknowledgement mail will be sent, so display a summery of the script
					//order including the total price of the order
					$buttonname = "send";
				}
					
				$err_m = true;	//true, if the mail address is invalid
				$err_n = true;	//true, if the family name is invalid
				$err_f = true;	//true, if the first name is invalid
				$err_s = true; 	//true, if the user doesn't have selected any scripts
				if (isset($_POST['vorname'])) //check the transmitted first name
				{
					$_POST['vorname'] = mysqli_real_escape_string($connection,$_POST['vorname']);
					$_POST['vorname'] = str_replace('"','',$_POST['vorname']);
					$_POST['vorname'] = strip_tags($_POST['vorname']);
					$_POST['vorname'] = trim($_POST['vorname']);
							
					if ($_POST['vorname'] != "")
					{
						//First name is valid
						$fina_default = $_POST['vorname'];
						$err_f = false;
						$correctfields++;
					}
				}
				if (isset($_POST['nachname'])) //check the transmitted last name
				{
					$_POST['nachname'] = mysqli_real_escape_string($connection,$_POST['nachname']);
					$_POST['nachname'] = str_replace('"','',$_POST['nachname']);
					$_POST['nachname'] = strip_tags($_POST['nachname']);
					$_POST['nachname'] = trim($_POST['nachname']);
					if ($_POST['nachname'] != "")
					{
						//Family name is valid
						$fana_default = $_POST['nachname'];
						$err_n = false;
						$correctfields++;
					}
				}
				if (isset($_POST['mail'])) //check the transmitted mail address
				{
					$_POST['mail'] = mysqli_real_escape_string($connection,$_POST['mail']);
					$_POST['mail'] = str_replace('"','',$_POST['mail']);
					$_POST['mail'] = strip_tags($_POST['mail']);
					$_POST['mail'] = trim($_POST['mail']);
							
					if ($_POST['mail'] != "")
					{
						//Mail address has been given, check if it's valid
						$_POST['mail'] = strtolower($_POST['mail']);
						$at = strpos($_POST['mail'],"@");
						
						if ($at != false AND strpos($_POST['mail'],".",$at))
						{
							//if the given mail address mets the scheme of valid mails,
							//accept it
							$mail_default = $_POST['mail'];
							$err_m = false;
							$correctfields++;
						}
					}
				}
				if (isset($_POST['script'])) //check the transmitted script IDs
				{
					/*if the user has selected at least one script,
					  store the selection (so that the scripts keeps
					  selected also if the order has to be displayed
					  again, because the user didn't fill out all
					  required fields)*/
							
					$err_s = false;
					$sid = $_POST['script'];
					$correctfields++;
							
					$idlist = implode(",",$sid);
								
					//avoid SQL-Injections and Cross Site Scripting
					$idlist = mysqli_real_escape_string($connection, $idlist); //avoid SQL-Injections
					$idlist = strip_tags($idlist);
				}
					
				if (isset($_POST['ok']))
				{					
					//if the order form has been sent already, first escape mysql-statements in the transmitted
					//parameters to avoid SQL-Injections, escape HTML- and PHP-Tags to avoid Cross Site Scripting
					//and check if all fields are transmitted and valid												
					if ($correctfields == 4)
					{
						//if all given fields are valid, check if the given name and mail address
						//already exists in the database
						$query = "
						SELECT BID, Nachname, Vorname, Mail
						FROM besteller
						WHERE Vorname=\"".$_POST['vorname']."\"
						AND Nachname=\"".$_POST['nachname']."\"
						LIMIT 1";
									
						$result = $connection->query($query);
								
						function order_script()
						{
							//create an entry in the table "SkriptBestellung"
							//for every script the customer wishes to order
							// -> finish of the script ordering
								
							//first compare the transmitted IDs with the script IDs in the database
							global $row, $idlist, $connection, $connection2, $ackcode, $send_ack, $from, $_POST, $_SERVER,$sem, $bid;
										
							$no_err = true;
							$ackcode = mt_rand();	//generate acknowledgement code in the range from 0 to 4.294.967.295
																				
							$query = "
							SELECT SID, Skript.Preis AS Preis
							FROM Skript
							WHERE SID IN (".$idlist.")
							AND Verfuegbar = 1 ";
			
							if ($sem == 0)
							{
								$cond = "AND Sommersemester = 1";
							}
							else
								$cond = "AND Wintersemester = 1";
			
							$query = $query.$cond;									
							$result = $connection->query($query);
								
							$total = 0.0; //contains total price to be displayed in the acknowledgement mail
								
							while ($row = $result->fetch_assoc())
							{
								//For all valid SIDs, create a new entry in
								//SkriptBestellung to finish the order of the
								//script
								$total = $total+$row['Preis'];	//calculate total price (needed for acknowledgement mail)
								$query2 = "
								INSERT INTO SkriptBestellung(SID, BID, Bestelldatum, Druckdatum, gedruckt, ACKCode, Bestaetigt)
								VALUES (".$row['SID'].", ".$bid.", NOW(), NOW(), 0,".$ackcode.",0)";
																			
								//Insert row
								$result2 = $connection2->query($query2);
								if ($result2 == false)
								{
									$no_err = false;
								}
							}
									
							if ($no_err == false)
							{
								//If an error occured while ordering some
								//of the selected scripts, display an error message
								echo "Fehler: Für einige der gewählten Skripte konnte die Bestellung nicht abgeschlossen<br>".
								"werden. Bitte prüfen Sie den <a href=\"".$_SERVER['SCRIPT_NAME']."?page=status\">Status Ihrer Skriptbestellung</a>, ".
								"um zu sehen, für welche<br>der gewählten Skripte die Bestellung abgeschlossen wurde.";
							}
							else
							{																			
								if ($send_ack == true)
								{
									//If no error occured while ordering the script
									//say it to the user
									echo "Die Bestellung der Skripte ist abgeschlossen. Eine E-Mail wurde an die von Ihnen angegebene Mail-Adresse ".
									"versandt. Bitte klicken Sie den darin enthaltenen Link an, um Ihre Skriptbestellung zu bestätigen, damit wir ".
									"diese drucken können.";
										
									//Read the ordered scripts from database
									$query = "SELECT SID, Modulbezeichnung, CONCAT(FORMAT(Skript.Preis,2,'de_DE'),' €') AS Preis, Name
									FROM Skript INNER JOIN Dozent
									ON Skript.DID = Dozent.DID
									WHERE SID IN (".$idlist.")
									AND Verfuegbar = 1 ";
			
									if ($sem == 0)
									{
										$cond = "AND Sommersemester = 1";
									}
									else
										$cond = "AND Wintersemester = 1";
			
									$query = $query.$cond;
										
									$ordered = "";
									$result = $connection->query($query);
									while($row = $result->fetch_assoc())
									{
										$ordered = $ordered.$row['Modulbezeichnung']."(".$row['Name'].") - ".$row['Preis']."\n";
									}
																					
									//Send an acknowledgement mail
									$subject = "Skriptbestellung wurde entgegen genommen.";
									
									$message = 	"Sehr geehrte(r) Frau/Herr ".$_POST['nachname'].",\n".
												"Sie haben folgende Skripte zu einem Gesamtpreis von ".number_format($total,2,',','.')." € bei uns bestellt:\n".$ordered.
												"\nBitte bestätigen Sie Ihre Skriptbestellung, in dem Sie folgenden Link in die Adresszeile Ihres Browsers kopieren:\n".
												$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']."?page=ack&bid=".$bid."&code=".$ackcode.
												"\n\nMit freundlichen Grüßen\n".
												"Ihr Fachschaftsrat IMN";									
									
									$header = "";
									if (trim($from) != "")
									{
										$header = 	"From: Fachschaftsrat IMN <".$from.">\r\n".
													"Reply-To: ".$from."\r\n";
									}
									
									$header = 	$header.
												"Content-type: text/plain; charset=utf-8";
									
									mail($_POST['mail'], $subject, $message, $header);
								}
								else
								{
									//If no error occured while ordering the script
									//say it to the user
									echo "Die Bestellung der Skripte ist abgeschlossen. Bitte prüfen Sie ".
									"regelmäßig den <a href=\"".$_SERVER['SCRIPT_NAME']."?page=status\">Status</a> ".
									"Ihrer Skriptbestellung.";
								}
							}
						}
								
						if ($row = $result->fetch_assoc())
						{
							//if the given name and mail address already exists, retrieve the BID
							//of the customer and call function order_script()
							$bid = $row['BID'];
							
							if ($_POST['mail'] != $row['Mail'])
							{
								//If the mail address of the customer has changed update the database
								$query = "
								UPDATE besteller
								SET Mail = \"".$_POST['mail']."\"
								WHERE BID = \"".$row['BID']."\"";
									
								$result = $connection->query($query);
							}
							
							order_script();
						}
						else
						{
							//If the given name an mail address not yet exists, first create a new
							//entry in table "Besteller"
							$query = "
							INSERT INTO Besteller(Nachname, Vorname, Mail)
							VALUES (\"".$_POST['nachname']."\",\"".$_POST['vorname']."\",\"".$_POST['mail']."\")";
							$result = $connection->query($query);
							if ($result == true)
							{
								//Now the customer has been add to the database -> retrieve his ID

								//ID des eingefügten Datensatzes ermitteln
								$result = $connection->query("SELECT LAST_INSERT_ID() FROM Besteller");
								$bid = implode($result->fetch_assoc());
								
								order_script();
							}
						}													
					}
					else
					{
						//Display an error message, because not all fields has been sent or some of the fields
						//are invalid
						if ($err_s == true)
						{
							//if the user didn't select any script
							$no_scripts = true;
							unset($_POST['ok']);
						}
						if ($err_f == true)
						{
							//If no first name has been given, display error message
							$fina_invalid = true;
							unset($_POST['ok']);
						}
						if ($err_n == true)
						{
							//if no family name has been given, display error message
							$fana_invalid = true;
							unset($_POST['ok']);
						}
						if ($err_m == true)
						{
							//if the mail address is invalid, display error message
							$mail_invalid = true;
							unset($_POST['ok']);
						}
					}		
				}
				else
				{
					if (isset($_POST['send']))
					{
						if ($err_m == false AND $err_n == false AND $err_f == false AND $err_s == false)
						{
							//Display overview page where the user have to acknowledge his script order
							echo "
								<b>Bitte prüfen Sie Ihre Angaben:</b>
								<form action=\"".$_SERVER['SCRIPT_NAME']."?page=order&cid=".$_GET['cid']."\" method=\"post\" accept-charset=\"UTF-8\">
								<br>Angaben zum Besteller:<br>
								<table>
									<colgroup>
										<col>
										<col>
									</colgroup>
									<tr>
										<td>Vorname: </td>
										<td>
											<input type=\"text\" size=\"25\" maxlength=\"50\" value=\"".$_POST['vorname']."\" disabled>
											<input name=\"vorname\" type=\"hidden\" value=\"".$_POST['vorname']."\">
										</td>
									</tr>
									<tr>
										<td>Nachname: </td>
										<td>
											<input type=\"text\" size=\"25\" maxlength=\"50\" value=\"".$_POST['nachname']."\" disabled>
											<input name=\"nachname\" type=\"hidden\" value=\"".$_POST['nachname']."\">
										</td>
									</tr>
									<tr>
										<td>E-Mail: </td>
										<td>
											<input type=\"text\" size=\"25\" maxlength=\"150\" value=\"".$_POST['mail']."\" disabled>
											<input name=\"mail\" type=\"hidden\" value=\"".$_POST['mail']."\">
										</td>
									</tr>
								</table>
								<br>
								Bestellte Skripte:<br>";
							
							$query = "
							SELECT DISTINCT Studiengang.StID, Skript.SID, Skript.Modulbezeichnung, Skript.Preis AS Preis, Dozent.Name AS Tutor 
							FROM (
							Skript INNER JOIN Dozent
							ON Skript.DID = Dozent.DID
							) INNER JOIN (
							StudiengangSkript INNER JOIN Studiengang
							ON StudiengangSkript.StID = Studiengang.StID)
							ON Skript.SID = StudiengangSkript.SID
							WHERE Verfuegbar = 1 
							AND Studiengang.StID='".$_GET['cid']."' ";
			
							if ($sem == 0)
							{
								$cond = "AND Sommersemester = 1";
							}
							else
								$cond = "AND Wintersemester = 1";
			
							$query = $query.$cond." AND Skript.SID in (".$idlist.") ORDER BY Modulbezeichnung ASC";
							$result = $connection->query($query);
							echo mysqli_error($connection);
							$total = 0.0;
							
							while($row = $result->fetch_assoc())
							{
								$price = $row['Preis'];
								echo "
								<input type=\"checkbox\" checked disabled>&#160;&#160;".$row['Modulbezeichnung']." (".$row['Tutor'].") - ".number_format($price,2,',','.')." €<br>
								<input type=\"hidden\" name=\"script[]\" value=\"".$row['SID']."\">";	
								$total = round($total+$price,2,PHP_ROUND_HALF_UP);
							}
							
							echo "
								<br>
								<b>Gesamtpreis: ".number_format($total,2,',','.')." €</b>
								<table style=\"width:100%\">
									<colgroup>
										<col>
										<col>
										<col>
									</colgroup>
									<tr>
										<td style=\"width: 33%\"></td>
										<td style=\"text-align: center; width: 33%\">
											<input type=\"submit\" name=\"ok\" value=\"Best&auml;tigen\" style=\"width: 6em\">
										</td>
										<td style=\"width: 33%\"></td>
									</tr>
								</table>
								</form>";
						}
						else
						{
							unset($_POST['send']);
							//Display an error message, because not all fields has been sent or some of the fields
							//are invalid
							if ($err_s == true)
							{
								//if the user didn't select any script
								$no_scripts = true;
								unset($_POST['ok']);
							}
							if ($err_f == true)
							{
								//If no first name has been given, display error message
								$fina_invalid = true;
								unset($_POST['ok']);
							}
							if ($err_n == true)
							{
								//if no family name has been given, display error message
								$fana_invalid = true;
								unset($_POST['ok']);
							}
							if ($err_m == true)
							{
								//if the mail address is invalid, display error message
								$mail_invalid = true;
								unset($_POST['ok']);
							}
						}
					}
				}
				if (!isset($_POST['send']) AND !isset($_POST['ok']))
				{
					//displays the form if it hasn't been sent already
					echo "
								<h3>Skripte für ".$row['Bezeichnung']."</h3>";
							
					$query = "
					SELECT DISTINCT Studiengang.StID, Skript.SID, Skript.Modulbezeichnung, CONCAT(FORMAT(Skript.Preis,2,'de_DE'),' €') AS Preis, Dozent.Name AS Tutor 
					FROM (
					Skript INNER JOIN Dozent
					ON Skript.DID = Dozent.DID
					) INNER JOIN (
					StudiengangSkript INNER JOIN Studiengang
					ON StudiengangSkript.StID = Studiengang.StID)
					ON Skript.SID = StudiengangSkript.SID
					WHERE Verfuegbar = 1 ";
			
					if ($sem == 0)
					{
						$cond = "AND Sommersemester = 1";
					}
					else
						$cond = "AND Wintersemester = 1";
			
					$query = $query.$cond." AND Studiengang.StID = '".$_GET['cid']."'
					ORDER BY Modulbezeichnung ASC";
						
					$result = $connection->query($query);
					$cid = $_GET['cid'];	//curse id
						
					echo "
								<form action=\"".$_SERVER['SCRIPT_NAME']."?page=order&cid=".$_GET['cid']."\" method=\"post\" accept-charset=\"UTF-8\">
									Bitte wählen Sie die Skripte aus, die Sie bestellen möchten:<br>";
					$index = 1;
					while($row = $result->fetch_assoc())
					{
						if (isset($sid) AND in_array($row['SID'],$sid))
						{
							/*	if not all required fields of the order form were filled
								out and the current script ID was in the selection,
								mark it as selected
							 */
							$sel = "checked";
						}
						else
						{
							/*	the current script ID was not in the selection,
								so mark it as unselected
								*/
							$sel = "";
						}
						echo "
									<input type=\"checkbox\" name=\"script[]\" value=\"".$row['SID']."\" ".$sel." tabindex=\"".$index."\" onchange=\"display_total_price()\">&#160;&#160;".$row['Modulbezeichnung']." (".$row['Tutor'].") - <span id=\"p".$index."\">".$row['Preis']."</span><br>";
						$index++;
					}
					echo "
								<p id=\"totalprice\">&nbsp;</p>
								<br>Bitte geben Sie hier Ihre Daten ein, um die Bestellung abzuschließen:<br>
								<table>
									<colgroup>
										<col>
										<col>
									</colgroup>
									<tr>";
					$index++;
					echo "
										<td>Vorname: </td>
										<td><input name=\"vorname\" type=\"text\" size=\"25\" maxlength=\"50\" value=\"".$fina_default."\" tabindex=\"$index\" required></td>
									</tr>
									<tr>";
					$index++;
					echo "
										<td>Nachname: </td>
										<td><input name=\"nachname\" type=\"text\" size=\"25\" maxlength=\"50\" value=\"".$fana_default."\" tabindex=\"$index\" required></td>
									</tr>
									<tr>";
					$index++;
					echo "
										<td>E-Mail: </td><td><input name=\"mail\" type=\"text\" size=\"25\" maxlength=\"150\" value=\"".$mail_default."\" tabindex=\"$index\" required></td>
									</tr>
								</table>
								<table style=\"width: 100%\">
									<colgroup>
										<col>
										<col>
										<col>
									</colgroup>
									<tr>
										<td style=\"width: 33%\"></td>
										<td style=\"text-align: center; width: 33%\">
											<input type=\"submit\" name=\"".$buttonname."\" value=\"Absenden\" style=\"width: 6em\">
										</td>
										<td style=\"width: 33%\"></td>
									</tr>
								</table><br>
								</form>
							</td>
						</tr>
					</table>
				</div>";

					if (isset($no_scripts) OR isset($fina_invalid) OR isset($fana_invalid) OR isset($mail_invalid))
					{
						echo "<p class=\"error\">";
					}
						
					if (isset($no_scripts) AND $no_scripts == true)
					{
						echo "Fehler: Sie müssen mindestens ein Skript auswählen,
								dass Sie bestellen möchten<br>";
					}
						
					if (isset($fina_invalid) AND $fina_invalid == true)
					{
						echo "Fehler: Bitte geben Sie Ihren Vornamen ein.<br>";
					}
						
					if (isset($fana_invalid) AND $fana_invalid == true)
					{
						echo "Fehler: Bitte geben Sie Ihren Nachnamen ein.<br>";
					}
						
					if (isset($mail_invalid) AND $mail_invalid == true)
					{
						echo "Fehler: Bitte geben Sie eine gültige Mail-Adresse ein.<br>";
					}
												
					if (isset($no_scripts) OR isset($fina_invalid) OR isset($fana_invalid) OR isset($mail_invalid))
					{
						echo "</p>";
					}
				}
				else
				{
					echo "
							</td>
						</tr>
					</table>
				</div>";
				}
			}
		}
		else
		{
			echo "<p style=\"margin-top: 6em\"></p>";
		}
		echo "
				<p><a href=\"".$_SERVER['SCRIPT_NAME']."?page=info\">Infos zum Bestelltool</a></p>
			</div>
			";
	}
	else
	{
		echo "
			<p>Bis zum Ende des Semesters werden keine neuen Skript-Bestellungen mehr angenommen.</p>
			<p>Skripte sind aber weiterhin beim FSR erhältlich. Kommt einfach vorbei oder fragt per Mail an <a href=\"javascript:decode_uri();\">skripte (at) fsr-imn.de</a> an.</p>
			<p>Falls ihr schon Skripte vorbestellt habt, könnt ihr deren <a href=\"".$_SERVER['SCRIPT_NAME']."?page=status\">Status prüfen.</a></p>
			<p><br>
			<a href=\"".$_SERVER['SCRIPT_NAME']."?page=info\">Infos zum Bestelltool</a></p>";
	}
?>