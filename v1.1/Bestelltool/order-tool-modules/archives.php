<?php
	if ($session_valid == true)
	{
		if (isset($_POST['send0']))
		{
			//Bestellungen sollen gelöscht werden
			$condition = "";
			$invalid = true;
			if (isset($_POST['no_ack']))
			{
				$condition = $condition."Bestaetigt = '0'";
				$invalid = false;
				if (isset($_POST['ncatched']))
				{
					$condition = $condition." OR ";
				}
			}
			if (isset($_POST['ncatched']))
			{
				$condition = $condition."gedruckt = '1'";
				$invalid = false;
			}
			if ($invalid == false)
			{
				//wurde mindestens eine der beiden Checkboxen ausgewählt,
				//werden nun die Daten verarbeitet.
				$days = $_POST['min_age'];
				$days = trim($days);
				$days = mysqli_real_escape_string($connection, $days);
				$days = str_replace("'",'',$days);
				$days = strip_tags($days);
							
				if ($days != "")
				{
					$error = true;
					$query = "
					DELETE FROM skriptbestellung
					WHERE ".$condition."
					AND DATEDIFF(NOW(),Bestelldatum) >= '".$days."'";
								
					$result = $connection->query($query);
					if ($result == true)
						$error = false;
				}
			}
		}
		if (isset($_POST['send1']))
		{
			//Mail soll geschickt werden
			if ($sem == 0)
			{
				$date = strtotime($beginn_summer_semester.$datum);
			}
			else
			{
				$date = strtotime($beginn_winter_semester.$datum);
			}
			$query = "
			SELECT SkriptBestellung.gedruckt AS gedruckt, SkriptBestellung.BID AS BID, DATE_FORMAT(SkriptBestellung.Bestelldatum,'%d.%m.%Y %H:%i:%s') AS Bestelldatum,
			Dozent.Name AS Name, Skript.Modulbezeichnung AS Modulbezeichnung, Skript.Preis AS Preis, Besteller.Nachname AS Nachname, Besteller.Mail AS Mail, COUNT(SkriptBestellung.SID) AS Anzahl
			FROM Besteller INNER JOIN (
			SkriptBestellung INNER JOIN (
			Skript INNER JOIN Dozent
			ON Skript.DID = Dozent.DID)
			ON SkriptBestellung.SID = Skript.SID)
			ON Besteller.BID = SkriptBestellung.BID
			WHERE UNIX_TIMESTAMP(Bestelldatum) < ".$date." AND gedruckt = '1'
			GROUP BY SkriptBestellung.SID";
						
			$printed = array();
						
			$subject = "Abholung Ihrer Skriptbestellung";
			$ordered = array();
			$name = array();
			$totalprice = array();
						
			$result = $connection->query($query);
			while($row = $result->fetch_assoc())
			{
				if (array_key_exists($row['Mail'],$ordered))
				{
					$ordered[$row['Mail']] = $ordered[$row['Mail']].$row['Modulbezeichnung']."(".$row['Name'].") - ".$row['Anzahl']." Exemplare\n";
				}
				else
				{
					$ordered[$row['Mail']] = $row['Modulbezeichnung']."(".$row['Name'].") - ".$row['Anzahl']." Exemplare\n";
				}
				$name[$row['Mail']] = $row['Nachname'];
				if (array_key_exists($row['Mail'],$totalprice))
				{
					$totalprice[$row['Mail']] = $totalprice[$row['Mail']]+($row['Preis']*$row['Anzahl']);
				}
				else
				{
					$totalprice[$row['Mail']] = $row['Preis']*$row['Anzahl'];
				}
			}
						
			foreach($name as $key => $value)
			{
				$subject = 	"Nicht abgeholte Skriptbestellung";
				$message = 	"Sehr geehrte(r) Frau/Herr ".$value.",\n".
							"Sie haben im vergangenen Semester folgende Skripte zu einem Gesamtpreis von ".number_format($totalprice[$key],2,',','.')." € bestellt:\n".
							$ordered[$key].
							"\nDiese wurden von uns gedruckt. Bitte holen Sie diese innerhalb der nächsten Tage ab, sonst verfällt Ihre Bestellung.\n".
							"\nMit freundlichen Grüßen\n".
							"Ihr Fachschaftsrat IMN";
				
				$header = "";
				if (trim($from) != "")
				{
					$header = 	"From: Fachschaftsrat IMN <".$from.">\r\n".
								"Reply-To: ".$from."\r\n";
				}
				$header = 	$header.
							"Content-type: text/plain; charset=utf-8";
				
				mail($key, $subject, $message, $header);
			}
						
			$action_finished = true;
		}
		if (isset($_POST['send2']))
		{
			//Bestellungen aus vorhergehenden Semestern sollen gelöscht werden
						
			if ($sem == 0)
			{
				$date = strtotime($beginn_summer_semester.$datum);
			}
			else
			{
				$date = strtotime($beginn_winter_semester.$datum);
			}
												
			$query = "
			DELETE FROM
			SkriptBestellung
			WHERE UNIX_TIMESTAMP(Bestelldatum) < ".$date;
						
			$connection->query($query);
			$action_finished = true;
		}
		if (isset($_POST['send3']))
		{
			//Bestellungen sollen vom Betreuer des Bestelltools bestätigt werden
			if (isset($_POST['ack']))
			{
				$idlist = implode(',',$_POST['ack']);
				$idlist = mysqli_real_escape_string($connection, $idlist);
				$idlist = str_replace("'",'',$idlist);
				$idlist = strip_tags($idlist);
						
				$query = "
				UPDATE skriptbestellung
				SET Bestaetigt = '1'
				WHERE SBID IN (".$idlist.")";
						
				$connection->query($query);
				
				$ch_applied = true;
			}
		}	
		echo "
				<form action=\"".$_SERVER["PHP_SELF"]."?page=archives\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"border: 1px solid black\">
					<colgroup>
						<col>
						<col>
						<col>
					</colgroup>
					<tr>
						<th></th>
						<th>&#160;Alter&#160;</th>
						<th>&#160;Aktion&#160;</th>
					</tr>
					<tr>
						<td><input name=\"no_ack\" type=\"checkbox\" tabindex=\"1\">&#160;unbestätigte&#160;<input name=\"ncatched\" type=\"checkbox\" tabindex=\"2\">&#160;gedruckte&#160;</td>
						<td><input name=\"min_age\" type=\"text\" size=\"1\" maxlength=\"10\" tabindex=\"3\">&#160;Tage&#160;<br></td>
						<td><input name=\"send0\" type=\"submit\" value=\"Bestellungen löschen\" style=\"width: 100%\" tabindex=\"4\"></td>
					</tr>
					<tr>
						<td colspan = \"3\"><br>Skriptbestellungen aus vorhergehenden Semestern:</td>
					</tr>
					<tr>				
						<td colspan = \"3\">
						<input name=\"send1\" type=\"submit\" value=\"Erinnerungsmail senden\" style=\"width: 50%\" tabindex=\"5\">".
						"<input name=\"send2\" type=\"submit\" value=\"Bestellungen löschen\" style=\"width: 50%\" tabindex=\"6\"></td>
					</tr>";
							
		if (isset($action_finished) && $action_finished == true)
		echo "
					<tr>
						<td colspan=\"3\" style=\"text-align: center; font-weight: bold\">Aktion ausgeführt</td>
					</tr>";
							
		echo "
				</table>
				</form>";
						
		if (isset($error) AND $error == true)
		{
			echo "<p>Aktion fehlgeschlagen</p>";
		}
						
		$index = 7;
		if ($script_ack == true)
		{
			//Alle Skriptbestellungen anzeigen, die noch nicht bestätigt wurden
			echo "
				<h2 style=\"margin-top: 2em\">Unbestätigte Skriptbestellungen</h2>";
			
			if (isset($ch_applied) AND $ch_applied == true)
			{
				echo "
				<p style=\"font-weight: bold; margin: 0px 0px\">Skriptbestellungen wurden bestätigt</p>";
			}
			else
			{
				echo "
				<p style=\"margin-top: 0px\">Hier werden alle Skriptbestellungen angezeigt, die vom Besteller noch nicht bestätigt wurden.</p>";
			}
				
			echo "
				<form action=\"".$_SERVER["PHP_SELF"]."?page=archives\" method=\"post\" accept-charset=\"UTF-8\">
				<table style=\"text-align: center; border: 1px solid black; border-collapse: collapse\">
					<colgroup>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<thead>
					<tr>
						<th colspan=\"3\" style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Besteller&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Modulbezeichnung&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Dozent&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\">&#160;Bestelldatum&#160;</th>
						<th style=\"border-style: none none solid none; border-width: 1px; border-color: black\"></th>
					</tr>
					</thead>
					<tbody>";
					
			$query = "
			SELECT SBID, Nachname, Vorname, Mail, Modulbezeichnung, DATE_FORMAT(Bestelldatum,'%d.%m.%Y %H:%i:%s') AS Bestelldatum, Name
			FROM besteller INNER JOIN (
			skriptbestellung INNER JOIN (
			skript INNER JOIN dozent
			ON skript.DID = dozent.DID)
			ON skriptbestellung.SID = skript.SID)
			ON besteller.BID = skriptbestellung.BID
			WHERE bestaetigt = '0'
			ORDER BY UNIX_TIMESTAMP(Bestelldatum) ASC, Nachname ASC, Modulbezeichnung ASC";
						
			$result = $connection->query($query);
			while ($row = $result->fetch_assoc())
			{
				echo "
					<tr>
						<td>&#160;".$row['Vorname']."&#160;</td>
						<td>&#160;".$row['Nachname']."&#160;</td>
						<td>&#160;".$row['Mail']."&#160;</td>
						<td>&#160;".$row['Modulbezeichnung']."&#160;</td>
						<td>&#160;".$row['Name']."&#160;</td>
						<td>&#160;".$row['Bestelldatum']."&#160;</td>
						<td>&#160;
							<input name=\"ack[]\" value=\"".$row['SBID']."\" type=\"checkbox\" tabindex=\"".$index."\">&#160;bestätigt&#160;
						</td>
					</tr>";
				$index = $index++;
			}
						
			echo "
					<tr>
						<td colspan=\"7\" style=\"text-align: right\">
							<input name=\"send3\" type=\"submit\" value=\"Bestellungen bestätigen\" tabindex=\"".$index."\">
						</td>
					</tr>
					</tbody>
				</table>
				</form>";
		}
	}
?>