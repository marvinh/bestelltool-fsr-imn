<?php
	echo "
			<div class=\"status_page\">";
			
	$fina_default = "";
	$fana_default = "";
			
	if (isset($_POST['send']))
	{
		//Form already has been sent
		$correctfields = 0;
		if (isset($_POST['vorname']))
		{
			$_POST['vorname'] = mysqli_real_escape_string($connection, $_POST['vorname']);
			$_POST['vorname'] = trim($_POST['vorname']);
			$_POST['vorname'] = str_replace('"','',$_POST['vorname']);
			$_POST['vorname'] = strip_tags($_POST['vorname']);
					
			if ($_POST['vorname'] != "")
			{
				$fina_default = $_POST['vorname'];
				$correctfields++;
			}
			else
			{
				$fina_invalid = true;
				unset($_POST['send']); 	//display form, because the transmitted first name is invalid
			}
		}
		else
		{
			$fina_invalid = true;
			unset($_POST['send']); 	//display form, because the transmitted first name is invalid
		}
		if (isset($_POST['nachname']))
		{
			$_POST['nachname'] = mysqli_real_escape_string($connection, $_POST['nachname']);
			$_POST['nachname'] = trim($_POST['nachname']);
			$_POST['nachname'] = str_replace('"','',$_POST['nachname']);
			$_POST['nachname'] = strip_tags($_POST['nachname']);
					
			if ($_POST['nachname'] != "")
			{
				$fana_default = $_POST['nachname'];
				$correctfields++;
			}
			else
			{
				$fana_invalid = true;
				unset($_POST['send']);	//display form, because the transmitted family name is unvalid
			}
		}
		else
		{
			$fana_invalid = true;
			unset($_POST['send']);	//display form, because the transmitted family name is unvalid
		}
								
		if ($correctfields == 2)
		{
			//first name and family name are valid -> query database
					
			$query = "
			SELECT DATE_FORMAT(Bestelldatum,'%d.%m.%Y %H:%i:%s') AS Bestelldatum, gedruckt, DATE_FORMAT(Druckdatum,'%d.%m.%Y %H:%i:%s') AS Druckdatum, Modulbezeichnung, 
			CONCAT(FORMAT(Skript.Preis,2,'de_DE'),' €') AS Preis, bestaetigt
			FROM Besteller INNER JOIN (
			SkriptBestellung INNER JOIN Skript
			ON SkriptBestellung.SID = Skript.SID)
			ON Besteller.BID = SkriptBestellung.BID
			WHERE Besteller.Vorname = \"".$fina_default."\"
			AND Besteller.Nachname = \"".$fana_default."\"
			ORDER BY UNIX_TIMESTAMP(Bestelldatum) DESC";
										
			//Now display the result of the query
					
			echo "
				<table style=\"border-style: solid; border-width: 1px; border-color: black;\">
					<colgroup>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<tr>
						<th>Modulbezeichnung&#160;&#160;</th>
						<th>Preis&#160;&#160;</th>
						<th>Datum&#160;&#160;</th>
						<th>Status&#160;&#160;</th>
					</tr>";
										
			$result = $connection->query($query);
			while ($row = $result->fetch_assoc())
			{
				echo "
					<tr style=\"vertical-align: top\">
						<td>".$row['Modulbezeichnung']."&#160;&#160;</td>
						<td>".$row['Preis']."&#160;&#160;</td>";
						
				if($row['gedruckt'] == 0)
				{
					if($row['bestaetigt'] == 0 and $send_ack == true)
					{
						//if the user didn't acknowledge the script order, display it
						echo "
						<td>".$row['Bestelldatum']."&#160;&#160;</td>
						<td>Sie haben Ihre Skriptbestellung noch nicht bestätigt</td>";
					}
					else
					{
						//display date of the order if the script has not yet been printed
						echo "
						<td>".$row['Bestelldatum']."&#160;&#160;</td>
						<td>Ihre Skriptbestellung wurde entgegen genommen.<br>Das Skript wurde noch nicht gedruckt.</td>";
					}
				}
				else
				{
					//display date of print if the script has been printed
					echo "
						<td>".$row['Druckdatum']."&#160;&#160;</td>
						<td>Ihr Skript wurde gedruckt und liegt im Büro<br>
							des Fachschaftsrates für Sie bereit.<br>
							<u>Bitte holen Sie es innerhalb der nächsten zwei<br>
							Wochen ab.</u>
						</td>";
				}
						
				echo "
					</tr>";
			}
					
			echo "
				</table>";
		}
	}
	if (!isset($_POST['send']))
	{
		//Form has not yet been sent
		echo "
				<h1>Skriptbestellung des FSR IMN</h1>
				<form action=\"".$_SERVER['SCRIPT_NAME']."?page=status\" method=\"post\" accept-charset=\"UTF-8\">
					Bitte geben Sie Ihren Vor- und Nachnamen ein:
				<table>
					<colgroup>
						<col>
						<col>
					</colgroup>
					<tr>
						<td>Vorname:&#160;&#160;</td>
						<td><input type=\"text\" size=\"25\" maxlength=\"50\" name=\"vorname\" value=\"".$fina_default."\" tabindex=\"1\" required></td>
					</tr>
					<tr>
						<td>Nachname:&#160;&#160;</td>
						<td><input type=\"text\" size=\"25\" maxlength=\"50\" name=\"nachname\" value=\"".$fana_default."\" tabindex=\"2\" required></td>
					</tr>
					<tr>
						<td style=\"text-align: center\" colspan=\"2\"><input name=\"send\" type=\"submit\" value=\"Status prüfen\"></td>
					</tr>
				</table>
				</form>";
				
		if (isset($fina_invalid) OR isset($fana_invalid))
		{
			echo "<p class=\"error\">";
		}
				
		if (isset($fina_invalid) AND $fina_invalid == true)
		{
			echo "Fehler: Bitte geben Sie Ihren Vornamen ein.<br>";
		}
		if (isset($fana_invalid) AND $fana_invalid == true)
		{
			echo "Fehler: Bitte geben Sie Ihren Nachnamen ein.<br>";
		}
				
		if (isset($fina_invalid) OR isset($fana_invalid))
		{
			echo "</p>";
		}
	}
	echo "			
			</div>";
			
	echo "";
?>