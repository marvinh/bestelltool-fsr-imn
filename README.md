# Bestelltool FSR IMN
I created this Ordertool around march 2015 for the student council of my university while I studied computer science. 
I have found the source code in my archives and decided to make it public on my Gitlab.
The ordertool is written in PHP, HTML5, CSS3 and a bit of Javascript. At the time of creation,
I decided that I wanted the order tool to function even if Javascript is disabled in the browser of the user, so all functions
that are essential for the Ordertool to work are implemented on the server side with PHP.

There existed two versions 1.0 and 1.1, either of which can be found in the according directory.

## Environment
The tool requires a server with PHP and a MySQL database installed. The documentation about how to set everything up
can be found in the "Dokumentation" folder within the folder of the according Ordertool version.